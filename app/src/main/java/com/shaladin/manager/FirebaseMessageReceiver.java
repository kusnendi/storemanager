package com.shaladin.manager;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.shaladin.manager.activity.customers.CustomersActivity;
import com.shaladin.manager.activity.orders.OrdersActivity;

import java.util.HashMap;

/**
 * Created by EliteBook on 8/27/2016.
 */
public class FirebaseMessageReceiver extends FirebaseMessagingService {
    private static final String TAG = "FirebaseListening";
    Intent intent;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: "+remoteMessage.getFrom());
        Log.e(TAG, "Message: "+remoteMessage.getNotification().getBody());

        HashMap<String, String> dataBundle = new HashMap<>();
        String event = remoteMessage.getData().get("event");
        String msgTitle= remoteMessage.getNotification().getTitle();
        String msgBody = remoteMessage.getNotification().getBody();

        dataBundle.put("event", event);
        dataBundle.put("title", msgTitle);
        dataBundle.put("body", msgBody);

        sendNotification(dataBundle);
    }

    //Generate Notification
    private void sendNotification(HashMap<String, String> dataBundle) {
        switch (dataBundle.get("event")) {
            case "registration" :
                intent = new Intent(this, CustomersActivity.class);
                break;
            case "order" :
                intent = new Intent(this, OrdersActivity.class);
                break;
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_store)
                .setContentTitle(dataBundle.get("title"))
                .setContentText(dataBundle.get("body"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }
}
