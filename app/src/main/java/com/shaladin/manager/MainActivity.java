package com.shaladin.manager;

import android.*;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.shaladin.manager.adapter.CustomGridViewAdapter;
import com.shaladin.manager.collection.UserAuthCollection;
import com.shaladin.manager.collection.UserCollection;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.User;
import com.shaladin.manager.model.UserAuth;
import com.shaladin.manager.utils.Base64;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Context context;
    CoordinatorLayout rootLayout;
    GridView gridView;
    ArrayList arrayList;
    CollapsingToolbarLayout collapsingToolbarLayout;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private LoginTask mAuthTask = null;
    private final String AuthPrefs = "AuthPrefs";
    private final String isAuth    = "isAuth";
    private final String UserId    = "UserId";
    private final String UserKey   = "UserKey";
    private final String Password  = "Password";
    private String Password2;

    private static final int REQUEST_PERMISSION_READ_STORAGE    = 1001;
    private static final int REQUEST_PERMISSION_WRITE_STORAGE   = 1002;

    //initialize authSharedPreferences
    SharedPreferences AuthUser;

    private UserAuthCollection userAuthCollection;
    private UserAuth userAuth;
    private UserCollection userCollection;
    private User user;

    public static String[] gridViewStrings = {
            "Pesanan",
            "Katalog",
            "Kategori",
            "Pelanggan",
            "Pengaturan",
            "Keluar"
    };

    public static int[] gridViewImages = {
            R.drawable.ic_list,
            R.drawable.ic_catalog,
            R.drawable.ic_category,
            R.drawable.ic_users,
            R.drawable.ic_settings,
            R.drawable.ic_exit
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AuthUser = getSharedPreferences(AuthPrefs, Context.MODE_PRIVATE);

        if (AuthUser.getBoolean(isAuth, false)) {
            //Authenticate User
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            //Check Permission Requirement
            initStoragePermission(1);

            gridView = (GridView) findViewById(R.id.grid);
            gridView.setAdapter(new CustomGridViewAdapter(this, gridViewStrings, gridViewImages));

            if(!AuthUser.getBoolean("Token", false)) {
                try {
                    JSONObject json = new JSONObject();
                    json.put("is_merchant", 1);
                    json.put("company_id", AuthUser.getInt("CompanyId", 0));
                    json.put("token", FirebaseInstanceId.getInstance().getToken());
                    new SubmitTokenTask().execute(ServiceUrl.TOKEN, json.toString());
                } catch (JSONException je) {
                    Log.i("Error", je.getLocalizedMessage());
                }

                SharedPreferences.Editor editor = AuthUser.edit();
                editor.putBoolean("Token", true);
                editor.commit();
            }

            initInstances();
        } else {
            //Login View
            setContentView(R.layout.activity_login);

            mLoginFormView = findViewById(R.id.login_form);
            mProgressView = findViewById(R.id.login_progress);
            mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

            mPasswordView = (EditText) findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == R.id.login || id == EditorInfo.IME_NULL) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });

            Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
            mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
//                TaskStackBuilder tsb = TaskStackBuilder.create(getApplicationContext());
//                tsb.addParentStack(LoginActivity.this);
//                tsb.addNextIntent(new Intent(LoginActivity.this, MainActivity.class));
//                tsb.startActivities();
//                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                finish();
//                startActivity(intent);

                }
            });
        }
    }

    private void initInstances() {
        rootLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(AuthUser.getString("Company", "Grosir Bersama"));
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError("This field is required");
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            String url= ServiceUrl.AUTH + "?email=" + email + "&password=" + Base64.encodeBase64(password);
            new LoginTask().execute(url);
            Password2 = Base64.encodeBase64(password);
//            mAuthTask = new UserLoginTask(email, password);
//            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private class LoginTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject response = HttpServices.getData(params[0]);
                return response;
            } catch (Exception e) {
                Log.e("TAG", "Error: "+e.getLocalizedMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            showProgress(false);

            if(response != null) {
                try {
                    boolean Auth = response.getBoolean("isAuth");
                    SharedPreferences.Editor AuthUserEditor = AuthUser.edit();
                    AuthUserEditor.putBoolean(isAuth, Auth);
                    if(Auth) { //authenticated
                        JSONObject userData = response.getJSONObject("data");

                        AuthUserEditor.putBoolean(isAuth, Auth);
                        AuthUserEditor.putString(UserKey, response.getString("key"));
                        AuthUserEditor.putInt(UserId, Integer.parseInt(userData.getString("user_id")));
                        AuthUserEditor.putString(Password, Password2);
                        AuthUserEditor.putInt("CompanyId", Integer.parseInt(userData.getString("company_id")));
                        AuthUserEditor.putString("Company", userData.getString("company"));
                        AuthUserEditor.putString("Address", userData.getString("address"));
                        AuthUserEditor.putBoolean("Token", false);
                        AuthUserEditor.commit();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        finish();
                        startActivity(intent);
                    } else { //unauthorized
                        String message = response.getString("data");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (JSONException je) {
                    Log.e("TAG", "Error: " + je.getLocalizedMessage());
                }
            } else {
                Toast.makeText(getApplicationContext(), "Invalid user credential!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    private class SubmitTokenTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                String result = HttpServices.postData(params[0], params[1]);
                return result;
            } catch (Exception ex) {
                Log.i("Error", ex.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(result != null) {
                System.out.println(result);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initStoragePermission(int type) {
        if(type == 1) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if(shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Permission to Read Storage", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_READ_STORAGE);
                }
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_READ_STORAGE);
            }
        }
        else {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if(shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Permission to Write Storage", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_STORAGE);
                }
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_READ_STORAGE) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initStoragePermission(2);
                Toast.makeText(this, "Permission granted.", Toast.LENGTH_LONG).show();
            } else {
//                Toast.makeText(this, "Permission denied.", Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == REQUEST_PERMISSION_WRITE_STORAGE) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "Permission Read Storage Denied", Toast.LENGTH_LONG).show();
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
