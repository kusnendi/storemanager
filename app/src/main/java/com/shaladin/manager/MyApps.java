package com.shaladin.manager;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.mocoo.hang.rtprinter.driver.Contants;
import com.mocoo.hang.rtprinter.driver.HsBluetoothPrintDriver;
import com.shaladin.manager.collection.StatusCollection;
import com.shaladin.manager.model.Status;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by EliteBook on 9/29/2016.
 */
public class MyApps extends Application {
    BluetoothDevice device;
    StateHandler stateHandler = new StateHandler();
    private int state = 0;
    private List<Status> statusess = new ArrayList<>();
    private BluetoothAdapter BA;
    private Set<BluetoothDevice> pairedDevice;
    private BluetoothDevice bluetoothDevice;
    public HsBluetoothPrintDriver hsBluetoothPrintDriver;

    @Override
    public void onCreate() {
        super.onCreate();
//        getPairedPrinterDevice();

        StatusCollection statusCollection = new StatusCollection(getApplicationContext());
        statusCollection.open();
        int statusCount = statusCollection.count();
        if ( statusCount == 0 ) {
            String url = ServiceUrl.STATUSES;
            new OrderStatusTask().execute(url);
        }
        statusCollection.close();

    }

    public synchronized BluetoothDevice getConnection() {
        HsBluetoothPrintDriver.getInstance().setHandler(stateHandler);

        if(state != 3) {
            hsBluetoothPrintDriver = HsBluetoothPrintDriver.getInstance();
            hsBluetoothPrintDriver.start();
            hsBluetoothPrintDriver.connect(bluetoothDevice);
        }

        return bluetoothDevice;
    }

    public void enableBluetooth() {
        BA = BluetoothAdapter.getDefaultAdapter();
        if(!BA.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            enableBluetooth.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(enableBluetooth, null);
            this.getConnection();
        }
    }

    private void getPairedPrinterDevice() {
        BA = BluetoothAdapter.getDefaultAdapter();
        pairedDevice = BA.getBondedDevices();
        for(BluetoothDevice bt : pairedDevice) {
            if(bt.getName().equals("RP58_BU")) {
                bluetoothDevice = bt;
                break;
            }
        }
    }

    public int getState() {
        return state;
    }

    public class StateHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);
            Bundle data = message.getData();

            switch (data.getInt("flag")) {
                case Contants.FLAG_STATE_CHANGE :
                    int _state = data.getInt("state");
                    System.out.println(String.valueOf(state));
                    if(_state == 17) {
                        state = 3;
                    }
                    break;
                case Contants.FLAG_FAIL_CONNECT :
                    System.out.println("Failed to Connect");
                    state = 0;
                    break;
                case Contants.FLAG_SUCCESS_CONNECT:
                    System.out.println("Connected");
                    state = 3;
                    break;
            }
        }
    }

    public int getCountStatus() {
        StatusCollection statusCollection = new StatusCollection(getApplicationContext());
        statusCollection.open();
        return statusCollection.count();
    }

    private class OrderStatusTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception ex) {
                Log.e("TAG", ex.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json){
            if(json != null) {
                System.out.println(json.toString());
                try {
                    boolean isError = json.getBoolean("error");

                    if(!isError) {
                        JSONObject response = json.getJSONObject("response");
                        JSONArray statuses  = response.getJSONArray("statuses");
                        int length = statuses.length();

                        if(length > 0) {
                            for (int i = 0; i < length; i++) {
                                JSONObject status = statuses.getJSONObject(i);
                                com.shaladin.manager.model.Status model = new com.shaladin.manager.model.Status();
                                model.setStatus(status.getString("status"));
                                model.setDescription(status.getString("description"));
                                statusess.add(model);
                            }

                            insertStatuses(statusess);
                        }

                    }
                } catch (JSONException je) {
                    Log.i(getString(R.string.app_name), je.getLocalizedMessage());
                }
            } else {
                Log.e("TAG", "Failed to get statuses from server!");
            }
        }
    }

    private void insertStatuses(List<Status> statuses) {
        StatusCollection statusCollection = new StatusCollection(getApplicationContext());
        statusCollection.open();

        for (Status status : statuses) {
            statusCollection.create(status);
        }

        statusCollection.close();
    }
}
