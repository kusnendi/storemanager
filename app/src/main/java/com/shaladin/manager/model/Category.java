package com.shaladin.manager.model;

/**
 * Created by EliteBook on 10/10/2016.
 */
public class Category {
    private long id;
    private int parentId;
    private int categoryId;
    private String description;
    private String status;

//    public Category(long id, int parentId, int categoryId, String description) {
//        this.id = id;
//        this.parentId   = parentId;
//        this.categoryId = categoryId;
//        this.description= description;
//    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
}
