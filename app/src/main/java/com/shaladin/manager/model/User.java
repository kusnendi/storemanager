package com.shaladin.manager.model;

/**
 * Created by EliteBook on 8/18/2016.
 */
public class User {
    private long id;
    private int userId;
    private String email;
    private String company;
    private int companyId;
    private String firstName;
    private String lastName;
    private String country;
    private int provincyId;
    private String provincyName;
    private int kabupatenId;
    private String kabupatenName;
    private int cityId;
    private String cityName;
    private String address;
    private String phone;
    private String postCode;
    private String status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getProvincyId() {
        return provincyId;
    }

    public void setProvincyId(int provincyId) {
        this.provincyId = provincyId;
    }

    public String getProvincyName() {
        return provincyName;
    }

    public void setProvincyName(String provincyName) {
        this.provincyName = provincyName;
    }

    public int getKabupatenId() {
        return kabupatenId;
    }

    public void setKabupatenId(int kabupatenId) {
        this.kabupatenId = kabupatenId;
    }

    public String getKabupatenName() {
        return kabupatenName;
    }

    public void setKabupatenName(String kabupatenName) {
        this.kabupatenName = kabupatenName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName){
        this.cityName = cityName;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public String getPostCode(){
        return postCode;
    }

    public void setPostCode(String postCode){
        this.postCode = postCode;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
