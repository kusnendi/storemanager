package com.shaladin.manager.model;

/**
 * Created by EliteBook on 9/1/2016.
 */
public class Order {
    private long id;
    private int orderId;
    private int userId;
    private double subtotal;
    private double discount;
    private double shippingCost;
    private double total;
    private int timestamp;
    private int paymentId;
    private int shippingId;
    private String bFirstname;
    private String bLastname;
    private String bStateId;
    private String bState;
    private String bCityId;
    private String bCity;
    private String bDistrictId;
    private String bDistrict;
    private String bAddress;
    private String bPhone;
    private String bPostCode;
    private String sFirstname;
    private String sLastname;
    private String sStateId;
    private String sState;
    private String sCityId;
    private String sCity;
    private String sDistrictId;
    private String sDistrict;
    private String sAddress;
    private String sPhone;
    private String sPostCode;
    private String status;
    private String notes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getShippingId() {
        return shippingId;
    }

    public void setShippingId(int shippingId) {
        this.shippingId = shippingId;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getsFisrtname() {
        return sFirstname;
    }

    public void setsFirstname(String firstname) {
        this.sFirstname = firstname;
    }

    public String getsLastname() {
        return sLastname;
    }

    public void setsLastname(String bLastname) {
        this.sLastname = bLastname;
    }

    public String getsStateId() {
        return sStateId;
    }

    public void setsStateId(String stateId) {
        this.sStateId = stateId;
    }

    public String getsState() {
        return sState;
    }

    public void setsState(String state) {
        this.sState = state;
    }

    public String getsCityId() {
        return sCityId;
    }

    public void setsCityId(String cityId) {
        this.sCityId = cityId;
    }

    public String getsCity() {
        return sCity;
    }

    public void setsCity(String city) {
        this.sCity = city;
    }

    public String getsDistrictId() {
        return sDistrictId;
    }

    public void setsDistrictId(String discrictId) {
        this.sDistrictId = discrictId;
    }

    public String getsDistrict() {
        return sDistrict;
    }

    public void setsDistrict(String district) {
        this.sDistrict = district;
    }

    public String getsAddress() {
        return sAddress;
    }

    public void setsAddress(String address) {
        this.sAddress = address;
    }

    public String getsPhone() {
        return sPhone;
    }

    public void setsPhone(String phone) {
        this.sPhone = phone;
    }

    public String getsPostCode() {
        return sPostCode;
    }

    public void setsPostCode(String postCode) {
        this.sPostCode = postCode;
    }

    public String getbFisrtname() {
        return bFirstname;
    }

    public void setbFirstname(String firstname) {
        this.bFirstname = firstname;
    }

    public String getbLastname() {
        return bLastname;
    }

    public void setbLastname(String bLastname) {
        this.bLastname = bLastname;
    }

    public String getbStateId() {
        return bStateId;
    }

    public void setbStateId(String stateId) {
        this.bStateId = stateId;
    }

    public String getbState() {
        return bState;
    }

    public void setbState(String state) {
        this.bState = state;
    }

    public String getbCityId() {
        return bCityId;
    }

    public void setbCityId(String cityId) {
        this.bCityId = cityId;
    }

    public String getbCity() {
        return bCity;
    }

    public void setbCity(String city) {
        this.bCity = city;
    }

    public String getbDistrictId() {
        return bDistrictId;
    }

    public void setbDistrictId(String discrictId) {
        this.bDistrictId = discrictId;
    }

    public String getbDistrict() {
        return bDistrict;
    }

    public void setbDistrict(String district) {
        this.bDistrict = district;
    }

    public String getbAddress() {
        return bAddress;
    }

    public void setbAddress(String address) {
        this.bAddress = address;
    }

    public String getbPhone() {
        return bPhone;
    }

    public void setbPhone(String phone) {
        this.bPhone = phone;
    }

    public String getbPostCode() {
        return bPostCode;
    }

    public void setbPostCode(String postCode) {
        this.bPostCode = postCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
