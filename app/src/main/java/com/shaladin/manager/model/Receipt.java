package com.shaladin.manager.model;

/**
 * Created by EliteBook on 10/25/2016.
 */
public class Receipt {
    public long id;
    public String storeName;
    public String address;
    public String notes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
