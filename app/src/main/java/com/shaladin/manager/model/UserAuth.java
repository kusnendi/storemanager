package com.shaladin.manager.model;

/**
 * Created by EliteBook on 8/18/2016.
 */
public class UserAuth {
    private long id;
    private int userId;
    private String secretKey;
    private String password;
    private int isAuth;

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public int getUserId(){
        return userId;
    }

    public void setUserId(int userId){
        this.userId = userId;
    }

    public String getSecretKey(){
        return secretKey;
    }

    public void setSecretKey(String secretKey){
        this.secretKey = secretKey;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsAuth(){
        return isAuth;
    }

    public void setIsAuth(int isAuth){
        this.isAuth = isAuth;
    }
}
