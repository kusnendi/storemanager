package com.shaladin.manager.utils;

/**
 * Created by EliteBook on 9/22/2016.
 */
public class Base64 {
    public static String encodeBase64(String plain) {
        String encoded = android.util.Base64.encodeToString(plain.getBytes(), android.util.Base64.DEFAULT);
        return encoded;
    }
}
