package com.shaladin.manager.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by EliteBook on 8/29/2016.
 */
public class InternetConnection {
    public static boolean checkConnection(Context context) {
        return ((ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
}
