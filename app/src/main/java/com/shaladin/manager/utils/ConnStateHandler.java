package com.shaladin.manager.utils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.mocoo.hang.rtprinter.driver.Contants;
import com.mocoo.hang.rtprinter.driver.HsBluetoothPrintDriver;

/**
 * Created by EliteBook on 9/28/2016.
 */

public class ConnStateHandler extends Handler {
    @Override
    public void handleMessage(Message message) {
        super.handleMessage(message);
        Bundle data = message.getData();

        switch (data.getInt("flag")) {
            case Contants.FLAG_STATE_CHANGE :
                int state = data.getInt("state");
                System.out.println(String.valueOf(state));
                break;
            case Contants.FLAG_FAIL_CONNECT :
                System.out.println("Failed to Connect");
                break;
            case Contants.FLAG_SUCCESS_CONNECT:
                System.out.println("Connected");
                HsBluetoothPrintDriver hsBluetoothPrintDriver = HsBluetoothPrintDriver.getInstance();
                hsBluetoothPrintDriver.SelftestPrint();
                break;
        }
    }
}
