package com.shaladin.manager.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import com.mocoo.hang.rtprinter.driver.HsBluetoothPrintDriver;

import java.util.Set;

/**
 * Created by EliteBook on 9/28/2016.
 */
public class PrintDriver {
    ConnStateHandler connStateHandler = new ConnStateHandler();
    private BluetoothAdapter BA;
    private Set<BluetoothDevice> pairedDevice;
    private BluetoothDevice bluetoothDevice;
    public HsBluetoothPrintDriver hsBluetoothPrintDriver;

    public PrintDriver() {
        HsBluetoothPrintDriver.getInstance().setHandler(connStateHandler);
    }

    public void connectBluetooth() {
        hsBluetoothPrintDriver = HsBluetoothPrintDriver.getInstance();
        hsBluetoothPrintDriver.start();
        hsBluetoothPrintDriver.connect(bluetoothDevice);
        hsBluetoothPrintDriver.WakeUpPritner();
        hsBluetoothPrintDriver.SelftestPrint();
    }

    public void getPaired() {
        BA = BluetoothAdapter.getDefaultAdapter();
        pairedDevice = BA.getBondedDevices();
        for(BluetoothDevice bt:pairedDevice) {
            if(bt.getName().equals("RP58_BU")) {
                bluetoothDevice = bt;
                break;
            }
        }
    }

    public boolean isNoConnection() {
        return hsBluetoothPrintDriver.IsNoConnection();
    }

    public void printString(String str) {
        hsBluetoothPrintDriver.getInstance();
        hsBluetoothPrintDriver.Begin();
        hsBluetoothPrintDriver.printString(str);
    }
}
