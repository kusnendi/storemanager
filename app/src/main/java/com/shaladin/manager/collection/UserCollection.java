package com.shaladin.manager.collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.manager.database.SQLiteHelper;
import com.shaladin.manager.model.User;

/**
 * Created by EliteBook on 8/18/2016.
 */
public class UserCollection {
    //database fields
    private SQLiteDatabase db;
    private SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_USER_ID, helper.COLUMN_EMAIL, helper.COLUMN_COMPANY_ID, helper.COLUMN_COMPANY, helper.COLUMN_FIRSTNAME, helper.COLUMN_LASTNAME,
    helper.COLUMN_COUNTRY, helper.COLUMN_PROVINCY_ID, helper.COLUMN_PROVINCY_NAME, helper.COLUMN_KABUPATEN_ID,
    helper.COLUMN_KABUPATEN_NAME, helper.COLUMN_CITY_ID, helper.COLUMN_CITY_NAME, helper.COLUMN_ADDRESS,
    helper.COLUMN_PHONE, helper.COLUMN_POSTCODE, helper.COLUMN_STATUS};

    public UserCollection(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public User createUser(int userId, String email, String firstname, String lastname, String country, int provincyId,
                           String provincyName, int kabupatenId, String kabupatenName, int cityId, String cityName,
                           String address, String phone, String postcode, String status ){
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_USER_ID, userId);
        values.put(helper.COLUMN_EMAIL, email);
        values.put(helper.COLUMN_FIRSTNAME, firstname);
        values.put(helper.COLUMN_LASTNAME, lastname);
        values.put(helper.COLUMN_COUNTRY, country);
        values.put(helper.COLUMN_PROVINCY_ID, provincyId);
        values.put(helper.COLUMN_PROVINCY_NAME, provincyName);
        values.put(helper.COLUMN_KABUPATEN_ID, kabupatenId);
        values.put(helper.COLUMN_KABUPATEN_NAME, kabupatenName);
        values.put(helper.COLUMN_CITY_ID, cityId);
        values.put(helper.COLUMN_CITY_NAME, cityName);
        values.put(helper.COLUMN_ADDRESS, address);
        values.put(helper.COLUMN_PHONE, phone);
        values.put(helper.COLUMN_POSTCODE, postcode);
        values.put(helper.COLUMN_STATUS, status);

        long insertedId = db.insert(helper.TABLE_USERS, null, values);
        Cursor cursor = db.query(helper.TABLE_USERS, allColumns, helper.COLUMN_ID+" = "
            + insertedId, null, null, null, null);

        cursor.moveToFirst();
        User newUser = cursorToUser(cursor);
        cursor.close(); db.close();
        return newUser;
    }

    public User updateUser(int user_id, String firstname, String lastname, String email, String phone) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_FIRSTNAME, firstname);
        values.put(helper.COLUMN_LASTNAME, lastname);
        values.put(helper.COLUMN_EMAIL, email);
        values.put(helper.COLUMN_PHONE, phone);
        //Update Content on Database
        db.update(helper.TABLE_USERS, values, helper.COLUMN_USER_ID+" = "+user_id, null);
        Cursor cursor = db.query(helper.TABLE_USERS, allColumns, helper.COLUMN_USER_ID+" = "
                + user_id, null, null, null, null);
        cursor.moveToFirst();
        User user = cursorToUser(cursor);
        return user;
    }

    public void deleteUser() {
        db.delete(helper.TABLE_USERS, null, null);
        db.close();
    }

    public User getUser(long id) {

        Cursor cursor = db.query(helper.TABLE_USERS, allColumns, helper.COLUMN_USER_ID+" = "
            + id, null, null, null, null);

        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            User user = cursorToUser(cursor);
            cursor.close(); db.close();
            return user;
        }
        return null;
    }

    public void deleteUser(User user) {
        long id = user.getId();
        db.delete(helper.TABLE_USERS, helper.COLUMN_ID + " = " + id, null);
        db.close();
    }

    private User cursorToUser(Cursor cursor){
        User user = new User();
        user.setId(cursor.getLong(0));
        user.setUserId(cursor.getInt(1));
        user.setEmail(cursor.getString(2));
        user.setFirstName(cursor.getString(3));
        user.setLastName(cursor.getString(4));
        user.setCountry(cursor.getString(5));
        user.setProvincyId(cursor.getInt(6));
        user.setProvincyName(cursor.getString(7));
        user.setKabupatenId(cursor.getInt(8));
        user.setKabupatenName(cursor.getString(9));
        user.setCityId(cursor.getInt(10));
        user.setCityName(cursor.getString(11));
        user.setAddress(cursor.getString(12));
        user.setPhone(cursor.getString(13));
        user.setPostCode(cursor.getString(14));
        user.setStatus(cursor.getString(15));
        return user;
    }
}
