package com.shaladin.manager.collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.manager.database.SQLiteHelper;
import com.shaladin.manager.model.Payment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 10/28/2016.
 */
public class PaymentCollection {
    //database fields
    SQLiteDatabase db;
    SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_COMPANY_ID, helper.COLUMN_PAYMENT_NAME, helper.COLUMN_DESCRIPTION,
    helper.COLUMN_INSTRUCTIONS, helper.COLUMN_STATUS};

    public PaymentCollection(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public boolean create(Payment payment) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_COMPANY_ID, payment.getCompanyId());
        values.put(helper.COLUMN_PAYMENT_NAME, payment.getPaymentName());
        values.put(helper.COLUMN_DESCRIPTION, payment.getDescription());
        values.put(helper.COLUMN_INSTRUCTIONS, payment.getInstructions());
        values.put(helper.COLUMN_STATUS, payment.getStatus());

        long id = db.insert(helper.TABLE_PAYMENTS, null, values);

        if(id != 0) return true;
        else return false;
    }

    public List<Payment> all(int companyId) {
        List<Payment> payments = new ArrayList<>();
        Cursor cursor = db.query(helper.TABLE_PAYMENTS, allColumns, helper.COLUMN_COMPANY_ID+"="+companyId, null, null, null, null);

        if(cursor.getCount() > 0) {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                payments.add(CursorToModel(cursor));
            }
            cursor.close();
            return payments;
        }
        else {
            return payments;
        }
    }

    public void delete(Payment payment) {
        long id = payment.getId();
        db.delete(helper.TABLE_PAYMENTS, helper.COLUMN_ID + "=" + id, null);
        db.close();
    }

    public void truncate() {
        db.delete(helper.TABLE_PAYMENTS, null, null);
        db.close();
    }

    public int count(int companyId) {
        Cursor cursor = db.query(helper.TABLE_PAYMENTS, allColumns, helper.COLUMN_COMPANY_ID+"="+companyId, null, null, null, null);
        return cursor.getCount();
    }

    private Payment CursorToModel(Cursor cursor) {
        Payment payment = new Payment();
        payment.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        payment.setCompanyId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_COMPANY_ID)));
        payment.setPaymentName(cursor.getString(cursor.getColumnIndex(helper.COLUMN_PAYMENT_NAME)));
        payment.setDescription(cursor.getString(cursor.getColumnIndex(helper.COLUMN_DESCRIPTION)));
        payment.setInstructions(cursor.getString(cursor.getColumnIndex(helper.COLUMN_INSTRUCTIONS)));
        payment.setStatus(cursor.getString(cursor.getColumnIndex(helper.COLUMN_STATUS)));
        return payment;
    }
}
