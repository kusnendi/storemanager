package com.shaladin.manager.collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.manager.database.SQLiteHelper;
import com.shaladin.manager.model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 8/24/2016.
 */
public class OrderCollection {
    //database fields
    SQLiteDatabase db;
    SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_ORDER_ID, helper.COLUMN_USER_ID, helper.COLUMN_SUBTOTAL, helper.COLUMN_DISCOUNT,
    helper.COLUMN_SHIPPING_COST, helper.COLUMN_TOTAL, helper.COLUMN_TIMESTAMP, helper.COLUMN_PAYMENT_ID, helper.COLUMN_SHIPPING_ID, helper.COLUMN_B_FIRSTNAME,
    helper.COLUMN_B_LASTNAME, helper.COLUMN_B_STATE_ID, helper.COLUMN_B_STATE, helper.COLUMN_B_CITY_ID, helper.COLUMN_B_CITY, helper.COLUMN_B_DISTRICT_ID,
    helper.COLUMN_B_DISTRICT, helper.COLUMN_B_ADDRESS, helper.COLUMN_B_PHONE, helper.COLUMN_B_POSTCODE, helper.COLUMN_S_FIRSTNAME, helper.COLUMN_S_LASTNAME,
    helper.COLUMN_S_STATE_ID, helper.COLUMN_S_STATE, helper.COLUMN_S_CITY_ID, helper.COLUMN_S_CITY, helper.COLUMN_S_DISTRICT_ID, helper.COLUMN_S_DISTRICT,
    helper.COLUMN_S_ADDRESS, helper.COLUMN_S_PHONE, helper.COLUMN_S_POSTCODE, helper.COLUMN_NOTES};

    public OrderCollection(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public boolean createOrder(Order order) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_ORDER_ID, order.getOrderId());
        values.put(helper.COLUMN_USER_ID, order.getUserId());
        values.put(helper.COLUMN_SUBTOTAL, order.getSubtotal());
        values.put(helper.COLUMN_DISCOUNT, order.getDiscount());
        values.put(helper.COLUMN_SHIPPING_COST, order.getShippingCost());
        values.put(helper.COLUMN_TOTAL, order.getTotal());
        values.put(helper.COLUMN_TIMESTAMP, order.getTimestamp());
        values.put(helper.COLUMN_PAYMENT_ID, order.getPaymentId());
        values.put(helper.COLUMN_SHIPPING_ID, order.getShippingId());
        values.put(helper.COLUMN_B_FIRSTNAME, order.getbFisrtname());
        values.put(helper.COLUMN_B_LASTNAME, order.getbLastname());
        values.put(helper.COLUMN_B_STATE_ID, order.getbStateId());
        values.put(helper.COLUMN_B_STATE, order.getbState());
        values.put(helper.COLUMN_B_CITY_ID, order.getbCityId());
        values.put(helper.COLUMN_B_CITY, order.getbCity());
        values.put(helper.COLUMN_B_DISTRICT_ID, order.getbDistrictId());
        values.put(helper.COLUMN_B_DISTRICT, order.getbDistrict());
        values.put(helper.COLUMN_B_ADDRESS, order.getbAddress());
        values.put(helper.COLUMN_B_PHONE, order.getbPhone());
        values.put(helper.COLUMN_B_POSTCODE, order.getbPostCode());
        values.put(helper.COLUMN_S_FIRSTNAME, order.getsFisrtname());
        values.put(helper.COLUMN_S_LASTNAME, order.getsLastname());
        values.put(helper.COLUMN_S_STATE_ID, order.getsStateId());
        values.put(helper.COLUMN_S_STATE, order.getsState());
        values.put(helper.COLUMN_S_CITY_ID, order.getsCityId());
        values.put(helper.COLUMN_S_CITY, order.getsCity());
        values.put(helper.COLUMN_S_DISTRICT_ID, order.getsDistrictId());
        values.put(helper.COLUMN_S_DISTRICT, order.getsDistrict());
        values.put(helper.COLUMN_S_ADDRESS, order.getsAddress());
        values.put(helper.COLUMN_S_PHONE, order.getsPhone());
        values.put(helper.COLUMN_S_POSTCODE, order.getsPostCode());
        values.put(helper.COLUMN_NOTES, order.getNotes());

        long id = db.insert(helper.TABLE_ORDERS, null, values);

        if(id != 0) return true;
        else return false;
    }

    public List<Order> allOrders() {
        List<Order> orders = new ArrayList<>();

        Cursor cursor = db.query(helper.TABLE_ORDERS, allColumns, null, null, null, null, null);
        if(cursor.getCount() > 0)
        {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                orders.add(cursorToOrder(cursor));
            }
            cursor.close();
            db.close();
            return orders;
        } else {
            db.close();
            return null;
        }
    }

    public Order getOrderById(int orderId) {
        Cursor cursor   = db.query(helper.TABLE_ORDERS, allColumns, helper.COLUMN_ORDER_ID+" = "+orderId, null, null, null, null);
        cursor.moveToFirst();
        Order order     = cursorToOrder(cursor);
        return order;
    }

    public void deleteOrder(Order order) {
        long id = order.getId();
        db.delete(helper.TABLE_ORDERS, helper.COLUMN_ID+" = "+id, null);
        db.close();
    }

    public void clearOrders() {
        db.delete(helper.TABLE_ORDERS, null, null);
        db.close();
    }

    private Order cursorToOrder(Cursor cursor) {
        Order order = new Order();
        order.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        order.setOrderId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_ORDER_ID)));
        order.setUserId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_USER_ID)));
        order.setSubtotal(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_SUBTOTAL)));
        order.setDiscount(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_DISCOUNT)));
        order.setShippingCost(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_SHIPPING_COST)));
        order.setTotal(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_TOTAL)));
        order.setTimestamp(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_TIMESTAMP)));
        order.setPaymentId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_PAYMENT_ID)));
        order.setShippingId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_SHIPPING_ID)));
        order.setbFirstname(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_FIRSTNAME)));
        order.setbLastname(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_LASTNAME)));
        order.setbStateId(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_STATE_ID)));
        order.setbState(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_STATE)));
        order.setbCityId(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_CITY_ID)));
        order.setsCity(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_CITY)));
        order.setbDistrictId(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_DISTRICT_ID)));
        order.setbDistrict(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_DISTRICT)));
        order.setbAddress(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_ADDRESS)));
        order.setbPhone(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_PHONE)));
        order.setbPostCode(cursor.getString(cursor.getColumnIndex(helper.COLUMN_B_POSTCODE)));
        order.setsFirstname(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_FIRSTNAME)));
        order.setsLastname(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_LASTNAME)));
        order.setsStateId(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_STATE_ID)));
        order.setsState(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_STATE)));
        order.setsCityId(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_CITY_ID)));
        order.setsCity(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_CITY)));
        order.setsDistrictId(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_DISTRICT_ID)));
        order.setsDistrict(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_DISTRICT)));
        order.setsAddress(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_ADDRESS)));
        order.setsPhone(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_PHONE)));
        order.setsPostCode(cursor.getString(cursor.getColumnIndex(helper.COLUMN_S_POSTCODE)));
        order.setNotes(cursor.getString(cursor.getColumnIndex(helper.COLUMN_NOTES)));
        return order;
    }
}
