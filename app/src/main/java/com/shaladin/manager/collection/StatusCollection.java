package com.shaladin.manager.collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.manager.database.SQLiteHelper;
import com.shaladin.manager.model.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 10/5/2016.
 */
public class StatusCollection {
    //database fields
    SQLiteDatabase db;
    SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_STATUS, helper.COLUMN_DESCRIPTION};

    public StatusCollection(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public boolean create(Status status) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_STATUS, status.getStatus());
        values.put(helper.COLUMN_DESCRIPTION, status.getDescription());

        long id = db.insert(helper.TABLE_STATUS, null, values);

        if( id != 0) return true;
        else return false;
    }

    public List<Status> all() {
        List<Status> statuses = new ArrayList<>();
        Cursor cursor = db.query(helper.TABLE_STATUS, allColumns, null, null, null, null, null);

        if(cursor.getCount() > 0) {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                statuses.add(CursorToModel(cursor));
            }
            cursor.close();
            return statuses;
        } else {
            return null;
        }
    }

    public void delete(Status status) {
        long id = status.getId();
        db.delete(helper.TABLE_STATUS, helper.COLUMN_ID + "=" + id, null);
        db.close();
    }

    public void clear() {
        db.delete(helper.TABLE_STATUS, null, null);
        db.close();
    }

    public int count() {
        Cursor cursor = db.query(helper.TABLE_STATUS, allColumns, null, null, null, null, null);
        return cursor.getCount();
    }

    private Status CursorToModel(Cursor cursor) {
        Status status = new Status();
        status.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        status.setStatus(cursor.getString(cursor.getColumnIndex(helper.COLUMN_STATUS)));
        status.setDescription(cursor.getString(cursor.getColumnIndex(helper.COLUMN_DESCRIPTION)));
        return status;
    }
}
