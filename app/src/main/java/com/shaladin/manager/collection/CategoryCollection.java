package com.shaladin.manager.collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.manager.database.SQLiteHelper;
import com.shaladin.manager.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 10/11/2016.
 */
public class CategoryCollection {
    //database fields
    SQLiteDatabase db;
    SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_PARENT_ID, helper.COLUMN_CATEGORY_ID, helper.COLUMN_DESCRIPTION};

    public CategoryCollection(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public List<Category> all() {
        List<Category> categories = new ArrayList<>();
        Cursor cursor = db.query(helper.TABLE_CATEGORIES, allColumns, null, null, null, null, null);

        if(cursor.getCount() > 0) {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                categories.add(cursorToObject(cursor));
            }
            cursor.close();
            return categories;
        } else {
            return categories;
        }
    }

    public boolean create(Category category) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_PARENT_ID, category.getParentId());
        values.put(helper.COLUMN_CATEGORY_ID, category.getCategoryId());
        values.put(helper.COLUMN_DESCRIPTION, category.getDescription());

        long id = db.insert(helper.TABLE_CATEGORIES, null, values);

        if(id != 0) return true;
        else return false;
    }

    public void delete(Category category) {
        long id = category.getId();
        db.delete(helper.TABLE_CATEGORIES, helper.COLUMN_ID + "=" + id, null);
        db.close();
    }

    public int count() {
        Cursor cursor = db.query(helper.TABLE_CATEGORIES, allColumns, null, null, null, null, null);
        return cursor.getCount();
    }

    private Category cursorToObject(Cursor cursor) {
        Category category = new Category();
        category.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        category.setParentId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_PARENT_ID)));
        category.setCategoryId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_CATEGORY_ID)));
        category.setDescription(cursor.getString(cursor.getColumnIndex(helper.COLUMN_DESCRIPTION)));
        return category;
    }
}
