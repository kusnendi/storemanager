package com.shaladin.manager.config;

/**
 * Created by EliteBook on 9/1/2016.
 */
public class ServiceUrl {
//    private static final String ROOT  = "http://api.grosirbersama.co.id/api/";
    private static final String ROOT  = "http://api.pasarikan.co/api/";
    public static final String AUTH   = ROOT+"auth";
    public static final String CHANGE_PASSWORD = ROOT+"changepassword/";
    public static final String ORDERS = ROOT+"orders";
    public static final String UPDATE_ORDERS = ROOT+"orders-update/";
    public static final String PRODUCTS = ROOT+"products";
    public static final String PRICE_GROUP = ROOT+"price-group";
    public static final String STATUSES = ROOT+"statuses";
    public static final String STATUS_MEMBERS = ROOT+"status-members";
    public static final String USER = ROOT+"users";
    public static final String USER_ACTIVATION = ROOT+"user-activation";
    public static final String USER_GROUP = ROOT+"companymembers";
    public static final String COMPANY = ROOT + "company/";
    public static final String COMPANY_PAYMENT = ROOT + "company-payments";
    public static final String USER_PROFILE = ROOT + "user-profile/";
    public static final String TOKEN = ROOT + "token";
}
