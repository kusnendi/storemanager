package com.shaladin.manager.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shaladin.manager.R;

/**
 * Created by EliteBook on 9/2/2016.
 */
public class ProductsHolder extends RecyclerView.ViewHolder {
    public ImageView productImage;
    public TextView productName;
    public TextView productStock;
    public TextView productPrice;

    public ProductsHolder(View parent) {
        super(parent);
        productImage = (ImageView) parent.findViewById(R.id.productImage);
        productName  = (TextView) parent.findViewById(R.id.productName);
        productStock = (TextView) parent.findViewById(R.id.productStock);
        productPrice = (TextView) parent.findViewById(R.id.productPrice);
    }
}
