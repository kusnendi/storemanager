package com.shaladin.manager.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.shaladin.manager.R;

/**
 * Created by EliteBook on 9/1/2016.
 */
public class OrdersHolder extends RecyclerView.ViewHolder {
    public TextView orderId;
    public TextView status;
    public TextView customerInfo;
    public TextView subTotal;

    public OrdersHolder(View parent) {
        super(parent);
        orderId = (TextView) parent.findViewById(R.id.TrxId);
        status  = (TextView) parent.findViewById(R.id.status);
        customerInfo = (TextView) parent.findViewById(R.id.customerInfo);
        subTotal= (TextView) parent.findViewById(R.id.subTotal);
    }
}