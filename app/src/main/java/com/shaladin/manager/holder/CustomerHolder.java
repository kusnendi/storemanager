package com.shaladin.manager.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.shaladin.manager.R;

import org.w3c.dom.Text;

/**
 * Created by EliteBook on 11/4/2016.
 */
public class CustomerHolder extends RecyclerView.ViewHolder {
    public TextView realName;
    public TextView description;
    public TextView groupName;
    public TextView balance;

    public CustomerHolder(View parent) {
        super(parent);

        realName    = (TextView) parent.findViewById(R.id.realName);
        description = (TextView) parent.findViewById(R.id.description);
        groupName   = (TextView) parent.findViewById(R.id.groupName);
        balance     = (TextView) parent.findViewById(R.id.credit);
    }
}
