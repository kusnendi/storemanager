package com.shaladin.manager.activity.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Store;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

public class ReceiptActivity extends AppCompatActivity implements View.OnClickListener {
    private final String AuthPrefs = "AuthPrefs";
    private int companyId;

    Context context;
    SharedPreferences StorePrefs;
    SharedPreferences UserAuth;
    SharedPreferences ReceiptPrefs;
    EditText Company;
    EditText Address;
    EditText Notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_activity_settings));
        getSupportActionBar().setSubtitle(getString(R.string.title_activity_receipt));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        ReceiptPrefs= getSharedPreferences("ReceiptPrefs", Context.MODE_PRIVATE);
        StorePrefs  = getSharedPreferences("StorePrefs", Context.MODE_PRIVATE);
        UserAuth    = getSharedPreferences(AuthPrefs, Context.MODE_PRIVATE);
        companyId   = UserAuth.getInt("CompanyId", 0);

        Company = (EditText) findViewById(R.id.store);
        Address = (EditText) findViewById(R.id.address);
        Notes   = (EditText) findViewById(R.id.notes);

        if(!StorePrefs.contains("Store")) {
            String url = ServiceUrl.COMPANY + companyId;
            new GetCompanyTask().execute(url);
        } else if(!ReceiptPrefs.contains("Store")) {
            SharedPreferences.Editor editorNota = ReceiptPrefs.edit();
            editorNota.putString("Store", StorePrefs.getString("Store", "Nama Toko"));
            editorNota.putString("Address", StorePrefs.getString("Address", "Alamat Toko"));
            editorNota.putString("Notes", "");
            editorNota.commit();
        }
        else {
            init();
        }

        Button Save = (Button) findViewById(R.id.actionSave);
        Save.setOnClickListener( this );
    }

    private void init() {
        Company.setText(ReceiptPrefs.getString("Store", "Nama Toko"));
        Address.setText(ReceiptPrefs.getString("Address", "Alamat Toko"));
        Notes.setText(ReceiptPrefs.getString("Notes", "Catatan Nota"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionSave :
                SharedPreferences.Editor editor = ReceiptPrefs.edit();
                if(!Company.getText().toString().equals(ReceiptPrefs.getString("Store", "NaN"))) {
                    editor.putString("Store", Company.getText().toString());
                }
                if(!Address.getText().toString().equals(ReceiptPrefs.getString("Address", "NaN"))) {
                    editor.putString("Address", Address.getText().toString());
                }
                if(!Address.getText().toString().equals(ReceiptPrefs.getString("Notes", "NaN"))) {
                    editor.putString("Notes", Notes.getText().toString());
                }

                editor.commit();
                Toast.makeText(context, "Data berhasil di perbarui !", Toast.LENGTH_LONG)
                        .show();
                break;
        }
    }

    private class GetCompanyTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(context);
            dialog.setTitle("Please wait...");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if (json != null) {
                SharedPreferences.Editor editStore = StorePrefs.edit();
                SharedPreferences.Editor editNota  = ReceiptPrefs.edit();
                try {
                    boolean isError = json.getBoolean("error");
                    if(!isError) {
                        JSONObject response = json.getJSONObject("response");
                        //store data to sharedPreference
                        editStore.putString("Store", response.getString("company"));
                        editStore.putString("Address", response.getString("address"));
                        editStore.putString("District", response.getString("city"));
                        editStore.putString("Province", response.getString("state"));
                        editStore.putString("Phone", response.getString("phone"));
                        editStore.putString("Email", response.getString("email"));
                        editStore.commit();

                        editNota.putString("Store", response.getString("company"));
                        editNota.putString("Address", response.getString("address"));
                        editNota.putString("Notes", "");
                        editNota.commit();
                        //set EditText
                        init();
                    } else {
                        //
                    }
                } catch (JSONException je) {
                    Log.i("Error", je.getLocalizedMessage());
                }
            } else {
                Toast.makeText(context, "Failed to fetch data!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
