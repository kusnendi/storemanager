package com.shaladin.manager.activity.products;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shaladin.manager.R;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.utils.HttpServices;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {
    public final String EXTRA_PRODUCT_ID = "product_id";
    public final String EXTRA_PRODUCT = "product";
    public final String EXTRA_AMOUNT  = "amount";
    public final String EXTRA_IMAGE   = "image";
    public final String EXTRA_PRICE   = "price";
    public final String EXTRA_VIEWED  = "viewed";
    public final String EXTRA_DETAIL  = "detail";
    public final String EXTRA_JSON    = "json";

    private int productId;
    private int productAmount;
    private double productBasePrice;
    private String productPrice;
    private String productName;
    private String productDescription;
    private String json;
    TextView description;
    String productImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        productId = intent.getIntExtra(EXTRA_PRODUCT_ID, 0);
        productName  = intent.getStringExtra(EXTRA_PRODUCT);
        productPrice = intent.getStringExtra(EXTRA_PRICE);
        productAmount   = intent.getIntExtra(EXTRA_AMOUNT, 0);
        int productViewed   = intent.getIntExtra(EXTRA_VIEWED, 0);
        productBasePrice    = intent.getDoubleExtra("base_price", 0);
        productImage        = intent.getStringExtra(EXTRA_IMAGE);


        getSupportActionBar().setTitle(productName);

        Picasso.with(this).load(productImage).placeholder(R.drawable.placeholder)
                .into((ImageView) findViewById(R.id.productImage));
        TextView pName = (TextView) findViewById(R.id.productName);
        pName.setText(productName);
        TextView pPrice= (TextView) findViewById(R.id.productPrice);
        pPrice.setText(productPrice);
        TextView amount = (TextView) findViewById(R.id.productAmount);
        amount.setText("Stock: " + productAmount);

        FloatingActionButton actionEdit = (FloatingActionButton) findViewById(R.id.fabEdit);
        actionEdit.setOnClickListener( this );
        // Get Detail Product
        new ProductDetailTask().execute(ServiceUrl.PRODUCTS+"/"+intent.getIntExtra(EXTRA_PRODUCT_ID, 0));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabEdit:
                Context context = v.getContext();
                Intent intent = new Intent(context, EditProductActivity.class);
                intent.putExtra(EXTRA_PRODUCT_ID, productId);
                intent.putExtra(EXTRA_PRODUCT, productName);
                intent.putExtra(EXTRA_AMOUNT, productAmount);
                intent.putExtra(EXTRA_DETAIL, productDescription);
                intent.putExtra(EXTRA_PRICE, (int) productBasePrice);
                intent.putExtra("display_price", productPrice);
                intent.putExtra(EXTRA_JSON, json);
                intent.putExtra("productImage", productImage);
                context.startActivity(intent);
                break;
        }
    }

    private class ProductDetailTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(ProductDetailActivity.this);
            dialog.setTitle("Please wait...");
            dialog.setMessage("Fetching data from server, please wait for a while");
            dialog.show();

        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                String url = params[0];
                JSONObject jsonObject = HttpServices.getData(url);

                if(jsonObject != null) {
                    //applyObjData(jsonObject);
                    return jsonObject;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject object) {
            dialog.dismiss();
            try {
                JSONObject response = object.getJSONObject("response");
                description = (TextView) findViewById(R.id.productDescription);
                description.setText(Html.fromHtml(response.getString("full_description")));
                productDescription = response.getString("full_description").replaceAll("<br>","\n");
                json = response.getJSONArray("price_group").toString();
            } catch (JSONException e) {
                e.getLocalizedMessage();
            }
        }
    }

}
