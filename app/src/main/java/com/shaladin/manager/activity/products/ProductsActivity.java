package com.shaladin.manager.activity.products;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.ProductsAdapter;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Product;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {
    private List<Product> productList = new ArrayList<>();
    private ProductsAdapter adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences UserAuth = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        int CompanyId = UserAuth.getInt("CompanyId", 0);
        String uri = ServiceUrl.PRODUCTS + "?company_id=" + CompanyId;
        new ProductsTask().execute(uri);
        adapter = new ProductsAdapter(this, productList);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_add_white_24dp);
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab :
                Context context = v.getContext();
                Intent intent = new Intent(context, CreateProductActivity.class);
                context.startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem menuItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Product> filteredProduct = filter(productList, newText);
        if (filteredProduct.size() > 0) {
            adapter.setFilter(filteredProduct);
            return true;
        }
        return  false;
    }

    private List<Product> filter(List<Product> models, String query) {
        query = query.toLowerCase();

        final List<Product> filteredProducts = new ArrayList<>();
        for(Product model : models) {
            final String text = model.getProductName().toLowerCase();
            if(text.contains(query)) {
                filteredProducts.add(model);
            }
        }

        adapter = new ProductsAdapter(recyclerView.getContext(), filteredProducts);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        return filteredProducts;
    }

    private class ProductsTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(ProductsActivity.this);
            dialog.setTitle("Please Wait");
            dialog.setMessage("Fetching data from server, please wait for a while...");
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject jsonObject = HttpServices.getData(params[0]);
                return jsonObject;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            dialog.dismiss();

            if(jsonObject != null) {
                try {
                    if(jsonObject.has("error") && !jsonObject.getBoolean("error")) {
                        JSONObject response = jsonObject.getJSONObject("response");
                        JSONArray products  = response.getJSONArray("products");
                        int size = products.length();
                        if(size > 0) {
                            for (int i = 0; i < size; i++) {
                                Product _product = new Product();
                                JSONObject product = products.getJSONObject(i);
                                int productId      = Integer.parseInt(product.getString("product_id"));
                                String productName = product.getString("product");
                                double price       = Double.parseDouble(product.getString("price"));
                                String status      = product.getString("status");
                                int stock          = Integer.parseInt(product.getString("amount"));
                                int viewed         = 0;//Integer.parseInt(product.getString("viewed"));
                                JSONObject main_pair = product.getJSONObject("main_pair");
                                JSONObject detailed  = main_pair.getJSONObject("detailed");
                                String image       = detailed.getString("image_path");

//                                _product.setId();
                                _product.setProductId(productId);
                                _product.setProductName(productName);
                                _product.setPrice(price);
                                _product.setStock(stock);
                                _product.setStatus(status);
                                _product.setViewed(viewed);
                                _product.setImage(image);

                                productList.add(_product);
                                adapter.notifyItemInserted(i);
                            }
                        }
                        else {
                            Toast.makeText(ProductsActivity.this, "Buat Product Baru Sekarang !", Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                } catch (JSONException jsonException) {
                    Log.e("TAG", jsonException.getLocalizedMessage());
                    Toast.makeText(ProductsActivity.this, "Oops, Something when wrong !", Toast.LENGTH_LONG)
                            .show();
                }
            } else {
                Toast.makeText(ProductsActivity.this, "Oops, Something when wrong !", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
