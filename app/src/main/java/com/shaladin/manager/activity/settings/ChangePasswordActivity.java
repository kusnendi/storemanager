package com.shaladin.manager.activity.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.utils.Base64;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private final String AuthPrefs = "AuthPrefs";

    Context context;
    SharedPreferences AuthUser;
    EditText currentPassword;
    EditText newPassword;
    EditText renewPassword;
    Button actionSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context  = this;
        AuthUser = getSharedPreferences(AuthPrefs, Context.MODE_PRIVATE);
        currentPassword = (EditText)findViewById(R.id.currentPassword);
        newPassword     = (EditText) findViewById(R.id.newPassword);
        renewPassword   = (EditText) findViewById(R.id.renewPassword);

        actionSave      = (Button) findViewById(R.id.actionSave);
        actionSave.setOnClickListener( this );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionSave :
                View focusView;
                String Password = AuthUser.getString("Password", "NaN").replaceAll("\\n\\s+","");
                String Password2= Base64.encodeBase64(String.valueOf(currentPassword.getText())).replaceAll("\\s+","");
                String NewPassword  = Base64.encodeBase64(String.valueOf(newPassword.getText()));
                String RenewPassword= Base64.encodeBase64(String.valueOf(renewPassword.getText()));

                if(!Password.equals(Password2)) {
                    currentPassword.setError("Password yang anda masukan salah!");
                    focusView = currentPassword;
                    focusView.requestFocus();
                } else {
                    if(NewPassword.isEmpty()) {
                        newPassword.setError("Password baru harus di isi!");
                        focusView = newPassword;
                        focusView.requestFocus();
                    } else if(NewPassword.length() < 6) {
                        newPassword.setError("Password minimal 6 karakter!");
                        focusView = newPassword;
                        focusView.requestFocus();
                    } else if(!NewPassword.equals(RenewPassword)) {
                        renewPassword.setError("Password baru tidak sama!");
                        focusView = renewPassword;
                        focusView.requestFocus();
                    } else {
                        String url= ServiceUrl.CHANGE_PASSWORD+AuthUser.getInt("UserId", 0);
                        new ChangePasswordTask().execute(url, NewPassword);
                    }
                }
                break;
        }
    }

    private class ChangePasswordTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(ChangePasswordActivity.this);
            dialog.setTitle("Working...");
            dialog.setMessage("Please wait we\'re working now...");
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String body = "{\"company_id\":\""+ AuthUser.getInt("CompanyId",0) + "\", \"password\":\"" + params[1] + "\"}";
                body = body.replaceAll("(\n)", "");
                String response = HttpServices.postData(params[0], body);
                return response;
            } catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(String response) {
            dialog.dismiss();

            View view = findViewById(R.id.container);
            if(response != null)
            {
                try {
                    JSONObject json = new JSONObject(response);
                    System.out.println(response);

                    if(json.has("user_id"))
                    {
                        String Password = Base64.encodeBase64(newPassword.getText().toString());
                        SharedPreferences.Editor editor = AuthUser.edit();
                        editor.putString("Password", Password);
                        editor.commit();

                        Snackbar.make(view, "Password Berhasil di Ganti", Snackbar.LENGTH_LONG)
                                .show();
                        //end current task
                        ChangePasswordActivity.this.finish();
                    }
                    else
                    {
                        Snackbar.make(view, "Terjadi Kesalahan, Password Gagal di Ganti", Snackbar.LENGTH_LONG)
                                .show();
                    }
                } catch (JSONException je) {
                    Log.e("TAG", " "+je.getLocalizedMessage());
                }
            }
            else {
                Snackbar.make(view, "Gagal Mengubah Password, Coba Lagi Nanti", Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }
}
