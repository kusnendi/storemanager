package com.shaladin.manager.activity.categories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.shaladin.manager.activity.categories.CreateCategoryActivity;
import com.shaladin.manager.R;
import com.shaladin.manager.adapter.CustomGridViewAdapter;
import com.shaladin.manager.collection.CategoryCollection;
import com.shaladin.manager.model.Category;

import java.util.List;

public class CategoriesActivity extends AppCompatActivity {
    private List<Category> categories;
    private CategoryCollection categoryCollection;

//    private int[] imageIds = {
//            R.drawable.ic_folder
//    };

    CoordinatorLayout rootLayout;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        categoryCollection = new CategoryCollection(this);
        categoryCollection.open();
        categories = categoryCollection.all();

        String[] _categories = new String[categories.size()];
        int[] imageIds = new int[categories.size()];
        if(categories.size() == 0) {
            TextView message = (TextView) findViewById(R.id.message);
            message.setVisibility(View.VISIBLE);
            message.setText(Html.fromHtml("Belum ada kategori !<br/>Silakan Tambah Kategori Baru."));
        } else {
            int i = 0;
            for (Category category : categories) {
                _categories[i] = category.getDescription();
                imageIds[i] = R.drawable.ic_folder;
                i++;
            }
        }

        rootLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator);
        gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new CustomGridViewAdapter(this, _categories, imageIds));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_add_white_24dp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, CreateCategoryActivity.class);
                context.startActivity(intent);
            }
        });
    }

}
