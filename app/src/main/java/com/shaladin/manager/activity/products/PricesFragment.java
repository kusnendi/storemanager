package com.shaladin.manager.activity.products;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.PricesAdapter;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Price;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PricesFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    public final String EXTRA_PRODUCT_ID = "product_id";
    public final String EXTRA_PRODUCT = "product";
    public final String EXTRA_AMOUNT  = "amount";
    public final String EXTRA_IMAGE   = "image";
    public final String EXTRA_PRICE   = "price";
    public final String EXTRA_VIEWED  = "viewed";

    private int productId;
    private int productAmount;
    private int productPrice;
    private String basePrices;
    private String productName;
    private String productDesc;
    private String priceGroup;
    private List<Price> prices = new ArrayList<>();
    private PricesAdapter pricesAdapter;
    private String typePrice;
    private int quantity;
    private int value;
    private View PriceDialog;

    JSONArray priceJSON;
    RecyclerView recyclerView;
    HashMap<String, String> TypeMap;

    public PricesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_prices, container, false);

        Intent intent   = getActivity().getIntent();
        productId       = intent.getIntExtra(EXTRA_PRODUCT_ID, 0);
        productAmount   = intent.getIntExtra(EXTRA_AMOUNT, 0);
        productPrice    = intent.getIntExtra(EXTRA_PRICE, 0);
        productName     = intent.getStringExtra(EXTRA_PRODUCT);
        productDesc     = intent.getStringExtra("detail");
        basePrices      = intent.getStringExtra("display_price");
        priceGroup      = intent.getStringExtra("json");

        TextView basePrice = (TextView) view.findViewById(R.id.basePrice);
        basePrice.setText(basePrices);

        TypeMap = new HashMap<>();
        TypeMap.put("Harga", "A");
        TypeMap.put("Persen", "P");

        try
        {

            priceJSON = new JSONArray(priceGroup);
            int size  = priceJSON.length();
            System.out.println(size);

            if(size > 0)
            {
                for (int i=0;i<size;i++) {
                    JSONObject jPrice = priceJSON.getJSONObject(i);

                    Price mPrice = new Price();
                    mPrice.setId(i+1);
                    mPrice.setProductId(jPrice.getInt("product_id"));
                    mPrice.setPrice(Double.parseDouble(jPrice.getString("price")));
                    mPrice.setDiscount(Integer.parseInt(jPrice.getString("percentage_discount")));
                    mPrice.setQuantity(Integer.parseInt(jPrice.getString("lower_limit")));
                    mPrice.setGroup(Integer.parseInt(jPrice.getString("usergroup_id")));

                    prices.add(mPrice);
                }
            }

        } catch (JSONException je) {
            Log.i("Error", je.getLocalizedMessage());
        }

        pricesAdapter = new PricesAdapter(getContext(), prices);
        recyclerView  = (RecyclerView) view.findViewById(R.id.recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(pricesAdapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        FloatingActionButton addPrice = (FloatingActionButton) view.findViewById(R.id.fab);
        addPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        return view;
    }

    private JSONObject processPrice2Json(int value, int quantity, String type) {
        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("product_id", productId);
            jsonObject.put("qty", quantity);
            jsonObject.put("usergroup_id", 0);
            jsonObject.put("type", type);
            jsonObject.put("price", productPrice);
            jsonObject.put("value", value);

            return jsonObject;
        } catch (JSONException je) {
            Log.i("Error", je.getLocalizedMessage());
            return null;
        }
    }

    private class SubmitPriceTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Mohon tunggu...");
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String response = HttpServices.postData(params[0], params[1]);
                return response;
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            dialog.dismiss();

            if(response != null) {
                Log.e("Response", response);
                Toast.makeText(getContext(), "Daftar harga berhasil di tambahkan!", Toast.LENGTH_LONG)
                        .show();
            } else {
                Log.e("Error:", "Request Error!");
            }
        }
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int i) {
            switch (i) {
                case DialogInterface.BUTTON_POSITIVE :

                    EditText qty = (EditText) PriceDialog.findViewById(R.id.quantity);
                    quantity = Integer.parseInt(qty.getText().toString());

                    EditText vle = (EditText) PriceDialog.findViewById(R.id.value);
                    value = Integer.parseInt(vle.getText().toString());

                    JSONObject json = processPrice2Json(value, quantity, typePrice);
                    String url = ServiceUrl.PRICE_GROUP;
                    new SubmitPriceTask().execute(url, json.toString());

                    break;
                case DialogInterface.BUTTON_NEGATIVE :
                    break;
            }
        }
    };

    private void showDialog() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        PriceDialog = inflater.inflate(R.layout.dialog_price_lists, null);
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(PriceDialog)
                .setNegativeButton("Batal", dialogClickListener)
                .setPositiveButton("Simpan", dialogClickListener)
                .create();
        dialog.show();

        Spinner selectType = (Spinner) dialog.findViewById(R.id.type);
        String[] _Type = {"Pilih Jenis", "Harga", "Persen"};
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, _Type) {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0) {
                    return false;
                }
                return true;
            }
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0) {
                    tv.setTextColor(Color.GRAY);
                }
                tv.setTextColor(Color.BLACK);
                return view;
            }
        };
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectType.setAdapter(typeAdapter);
        selectType.setOnItemSelectedListener( this );
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = parent.getItemAtPosition(position).toString();
        if(position != 0) {
            typePrice = TypeMap.get(selected);
            System.out.println(typePrice);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
