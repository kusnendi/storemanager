package com.shaladin.manager.activity.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.User;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

public class StoreActivity extends AppCompatActivity implements View.OnClickListener {
    private final String AuthPrefs = "AuthPrefs";
    private int companyId;
    private String companyName;

    Context context;
    SharedPreferences StorePrefs;
    SharedPreferences UserAuth;
    SharedPreferences ReceiptPrefs;
    EditText Store;
    EditText Address;
    EditText City;
    EditText State;
    EditText Phone;
    EditText Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_activity_settings));
        getSupportActionBar().setSubtitle(getString(R.string.title_activity_store));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        ReceiptPrefs= getSharedPreferences("ReceiptPrefs", Context.MODE_PRIVATE);
        StorePrefs  = getSharedPreferences("StorePrefs", Context.MODE_PRIVATE);
        UserAuth    = getSharedPreferences(AuthPrefs, Context.MODE_PRIVATE);
        companyId   = UserAuth.getInt("CompanyId", 0);
        companyName = UserAuth.getString("Company", "NaN");

        Store   = (EditText) findViewById(R.id.company);
        Address = (EditText) findViewById(R.id.address);
        City    = (EditText) findViewById(R.id.city);
        State   = (EditText) findViewById(R.id.province);
        Phone   = (EditText) findViewById(R.id.phone);
        Email   = (EditText) findViewById(R.id.email);

//        if(!StorePrefs.contains("Store")) {
//            String url = ServiceUrl.COMPANY + companyId;
//            new GetCompanyTask().execute(url);
//        } else {
//            init();
//        }
        String url = ServiceUrl.COMPANY + companyId;
        new GetCompanyTask().execute(url);
//        init();

        Button Save = (Button) findViewById(R.id.actionSave);
        Save.setOnClickListener( this );

    }

    private void init() {
        Store.setText(StorePrefs.getString("Store", "Nama Toko"));
        Address.setText(StorePrefs.getString("Address", "Alamat Toko"));
        City.setText(StorePrefs.getString("District", "Nama Kota"));
        State.setText(StorePrefs.getString("Province", "Nama Provinsi"));
        Phone.setText(StorePrefs.getString("Phone", "Nomor Telepon"));
        Email.setText(StorePrefs.getString("Email", "Alamat Email"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionSave :
                SharedPreferences.Editor editor = StorePrefs.edit();

                if(!Store.getText().toString().equals(StorePrefs.getString("Store", "NaN"))) {
                    editor.putString("Store", Store.getText().toString());
                }
                if(!Address.getText().toString().equals(StorePrefs.getString("Address", "NaN"))) {
                    editor.putString("Address", Address.getText().toString());
                }
                if(!City.getText().toString().equals(StorePrefs.getString("District", "NaN"))) {
                    editor.putString("District", City.getText().toString());
                }
                if(!State.getText().toString().equals(StorePrefs.getString("Province", "NaN"))) {
                    editor.putString("Province", State.getText().toString());
                }
                if(!Phone.getText().toString().equals(StorePrefs.getString("Phone", "NaN"))) {
                    editor.putString("Phone", Phone.getText().toString());
                }
                if(!Email.getText().toString().equals(StorePrefs.getString("Email", "NaN"))) {
                    editor.putString("Email", Email.getText().toString());
                }
                editor.commit();

                try {

                    JSONObject json = new JSONObject();
                    json.put("company", Store.getText().toString());
                    json.put("address", Address.getText().toString());
                    json.put("phone", Phone.getText().toString());
                    json.put("email", Email.getText().toString());

                    String url = ServiceUrl.COMPANY + companyId;
                    new UpdateCompanyTask().execute(url, json.toString());

                } catch (JSONException je) {
                    Log.i("Error", je.getLocalizedMessage());
                }
                //set notification alert
                Toast.makeText(context, "Data berhasil di perbarui !", Toast.LENGTH_LONG)
                        .show();
                break;
        }
    }

    private class UpdateCompanyTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(StoreActivity.this);
            dialog.setTitle("Please wait, updating data...");
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String result = HttpServices.putData(params[0], params[1]);
                return result;
            } catch (Exception e) {
                Log.i("TAG", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            if(result != null) {
                System.out.println(result);
                Toast.makeText(StoreActivity.this, "Data berhasil di update!", Toast.LENGTH_LONG)
                        .show();
                //return back
                StoreActivity.this.finish();
            }
        }
    }

    private class GetCompanyTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(context);
            dialog.setTitle("Please wait...");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if (json != null) {
                SharedPreferences.Editor editStore = StorePrefs.edit();
                SharedPreferences.Editor editNota  = ReceiptPrefs.edit();
                try {
                    boolean isError = json.getBoolean("error");
                    if(!isError) {
                        JSONObject response = json.getJSONObject("response");
                        //store data to sharedPreference
                        editStore.putString("Store", response.getString("company"));
                        editStore.putString("Address", response.getString("address"));
                        editStore.putString("District", response.getString("city"));
                        editStore.putString("Province", response.getString("state"));
                        editStore.putString("Phone", response.getString("phone"));
                        editStore.putString("Email", response.getString("email"));
                        editStore.commit();

                        editNota.putString("Store", response.getString("company"));
                        editNota.putString("Address", response.getString("address"));
                        editNota.putString("Notes", "");
                        editNota.commit();
                        //set EditText
                        init();
                    } else {
                        //
                    }
                } catch (JSONException je) {
                    Log.i("Error", je.getLocalizedMessage());
                }
            } else {
                Toast.makeText(context, "Failed to fetch data!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
