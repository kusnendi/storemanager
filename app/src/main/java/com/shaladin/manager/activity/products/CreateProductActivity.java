package com.shaladin.manager.activity.products;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.collection.CategoryCollection;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Category;
import com.shaladin.manager.utils.HttpServices;
import com.shaladin.manager.utils.ImageUtils;
import com.shaladin.manager.utils.RealPathUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreateProductActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final int REQUEST_CAMERA = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;
    private static final String http_image_demo = "http://www.grosirbersama.co.id/images/detailed/31/1359-250__blouse_titanium.jpg";
    private final CharSequence[] items = {"Take Photo", "From Gallery"};

    public SharedPreferences UserAuth;
    private Bitmap mBitmap;
    private ImageView iv_image;
    private Uri selectedImageUri;
    private String realPath   = "";
    private String http_image = "";
    private int CategoryId = 0;

    List<Category> ParentCategories = new ArrayList<>();
    HashMap<String, Integer> CategoriesMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tambah Produk");

        UserAuth = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ImageView addImage = (ImageView) findViewById(R.id.addImage);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooserDialog();
            }
        });
        Spinner selectCategories = (Spinner) findViewById(R.id.categories);

        CategoryCollection categoryCollection = new CategoryCollection(this);
        categoryCollection.open();
        List<Category> categories = categoryCollection.all();
        ArrayAdapter<String> categoriesAdapter;

        if(categories.size() > 0) {
            CategoriesMap = new HashMap<>();
            String[] list_categories = new String[categories.size()];
            int i = 0;
            for (Category category : categories) {
                list_categories[i] = category.getDescription();
                CategoriesMap.put(category.getDescription(), category.getCategoryId());
                i++;
            }

            categoriesAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, list_categories);
        } else {
            initBaseCategories();
            String[] _categories = {
                    "Pilih Parent",
                    "Fashion Wanita",
                    "Fashion Anak",
                    "Busana Muslim",
                    "Busana Pria",
                    "Batik",
                    "Tas dan Sepatu",
                    "Interior",
                    "Aksesories",
                    "Baju Tidur",
                    "Pakaian Olahraga",
                    "Pakaian Dalam"
            };
            categoriesAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, _categories);
        }

        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectCategories.setAdapter(categoriesAdapter);
        selectCategories.setOnItemSelectedListener( this );

        Button actionSave = (Button) findViewById(R.id.actionSave);
        actionSave.setOnClickListener(this);
    }

    private void initBaseCategories() {
        String[] _categories = {
                "Pilih Parent",
                "Fashion Wanita",
                "Fashion Anak",
                "Busana Muslim",
                "Busana Pria",
                "Batik",
                "Tas dan Sepatu",
                "Interior",
                "Aksesories",
                "Baju Tidur",
                "Pakaian Olahraga",
                "Pakaian Dalam"
        };

        int[] _ids = {0, 165, 166, 167, 168, 171, 175, 177, 178, 284, 289, 290};

        int j = 1;
        for (int i=0; i<_categories.length; i++) {
            Category category = new Category();
            category.setId(j);
            category.setParentId(0);
            category.setCategoryId(_ids[i]);
            category.setDescription(_categories[i]);
            j++;

            ParentCategories.add(category);
        }

        if(ParentCategories.size() > 0) {
            CategoriesMap = new HashMap<>();
            for(Category category : ParentCategories) {
                CategoriesMap.put(category.getDescription(), category.getCategoryId());
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_orders, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        switch (id) {
//            case android.R.id.home :
//                super.onBackPressed();
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void openFileChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        initCameraPermission();
                        break;
                    case 1:
                        initGalleryIntent();
                        break;
                    default:
                }
            }
        });
        builder.show();
    }

    private void initGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(this, "Permission to use Camera", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            initCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initCameraIntent();
            } else {
                Toast.makeText(this, "Permission denied by user", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediafile(1);
        selectedImageUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File getOutputMediafile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name)
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyHHdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                realPath = selectedImageUri.getPath();
            } else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                }
            }
            Log.d("TAG", "real path: " + realPath);
            //Upload Image to Server
            new UploadTask().execute(realPath);
            mBitmap = ImageUtils.getScaledImage(selectedImageUri, this);
            setImageBitmap(mBitmap);
        }

    }

    private void setImageBitmap(Bitmap bm) {
        iv_image = (ImageView) findViewById(R.id.addImage);
        iv_image.setImageBitmap(bm);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionSave :
                EditText iProduct = (EditText) findViewById(R.id.iProductName);
                EditText iPrice   = (EditText) findViewById(R.id.iPrice);
                EditText iStock   = (EditText) findViewById(R.id.iStock);
                EditText iMinQty  = (EditText) findViewById(R.id.iMinQty);
                EditText iWeight  = (EditText) findViewById(R.id.iWeight);
                EditText iDescription = (EditText) findViewById(R.id.iDescription);

                String image;
                if (!http_image.isEmpty()) image = http_image;
                else image = http_image_demo;

                String description = iDescription.getText().toString();
                String product = iProduct.getText().toString();
                if(TextUtils.isEmpty(product)) {
                    iProduct.setError("Wajib di isi!");
                    iProduct.requestFocus();
                } else if(TextUtils.isEmpty(iPrice.getText().toString())) {
                    iPrice.setError("Wajib di isi!");
                    iPrice.requestFocus();
                } else if(TextUtils.isEmpty(iStock.getText().toString())) {
                    iStock.setError("Wajib di isi!");
                    iStock.requestFocus();
                } else if(TextUtils.isEmpty(iMinQty.getText().toString())) {
                    iMinQty.setError("Wajib di isi!");
                    iMinQty.requestFocus();
                } else if(TextUtils.isEmpty(iWeight.getText().toString())) {
                    iWeight.setError("Wajib di isi!");
                    iWeight.requestFocus();
                } else if(TextUtils.isEmpty(description)) {
                    iDescription.setError("Wajib di isi!");
                    iDescription.requestFocus();
                } else {
                    int price = Integer.parseInt(iPrice.getText().toString());
                    int stock = Integer.parseInt(iStock.getText().toString());
                    int minQty= Integer.parseInt(iMinQty.getText().toString());
                    float weight= Float.parseFloat(iWeight.getText().toString());

                    String json = jsonProduct(UserAuth.getInt("CompanyId", 0), product, CategoryId, price, stock, weight, minQty, image, description);
                    new CreateProductTask().execute(ServiceUrl.PRODUCTS, json);
//                    System.out.println(json);
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.categories :
                CategoryId = CategoriesMap.get(parent.getItemAtPosition(position).toString());
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CreateProductTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getApplicationContext());
            dialog.setTitle("Please Wait");
            dialog.setMessage("Mohon tunggu, proses menyimpan product ke server.");
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String response = HttpServices.postData(params[0], params[1]);
                return response;
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            dialog.dismiss();

            if(response != null) {
                Log.e("Response", response);
                Intent intent = new Intent(getApplicationContext(), ProductsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                CreateProductActivity.this.finish();
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Produk baru berhasil di tambahkan!", Toast.LENGTH_LONG)
                        .show();
            } else {
                Log.e("Error:", "Request Error!");
            }
        }
    }

    private String jsonProduct(int companyId, String product, int categoryId, int price, int amount, float weight, int minQty, String image, String description) {
        String json = "{\"company_id\":\""+companyId+"\"," +
                "\"product\":\""+product+"\"," +
                "\"category_ids\":\""+categoryId+"\", " +
                "\"main_category\":\""+categoryId+"\", " +
                "\"price\":\""+price+"\", " +
                "\"list_price\":\""+price+"\", " +
                "\"status\":\"A\", " +
                "\"amount\":\""+amount+"\", " +
                "\"weight\":\""+weight+"\", " +
                "\"min_qty\":\""+minQty+"\", " +
                "\"image\":\""+image+"\", " +
                "\"short_description\":\""+description.replaceAll("[\\n]", "<br/>")+"\", " +
                "\"full_description\":\""+description.replaceAll("[\\n]", "<br/>")+"\"}";

        return json;
    }

    private String uploadImage(String filePath) {
        OkHttpClient client = new OkHttpClient();
        try {
            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("images", filePath, RequestBody.create(MEDIA_TYPE_JPG, new File(filePath)))
                    .build();
            Request request = new Request.Builder()
                    .url("http://api.grosirbersama.co.id/upload")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (Exception e) {
            Log.e("TAG", "Error: "+e.getLocalizedMessage());
        }
        return null;
    }

    private class UploadTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            Toast.makeText(getApplicationContext(), "Please wait, uploading image to server...", Toast.LENGTH_LONG)
                    .show();
        }
        @Override
        protected String doInBackground(String... params) {
            String response = uploadImage(params[0]);
            if(response != null) {
                return response;
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            if(response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equals("success")) {
                        http_image = jsonObject.getString("path");
                    }
                } catch (JSONException je) {
                    Log.e("TAG", je.getLocalizedMessage());
                }
                Toast.makeText(getApplicationContext(), "Upload image sukses", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), "Upload image gagal!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
