package com.shaladin.manager.activity.customers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.CustomerRequestAdapter;
import com.shaladin.manager.adapter.CustomersAdapter;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Customer;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListCustomersActivity extends AppCompatActivity {
    private List<Customer> customers = new ArrayList<>();
    private CustomersAdapter customersAdapter;
    private int groupId;
    private int companyId;
    private String groupName;

    RecyclerView recyclerView;
    Context context = ListCustomersActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_customers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        groupName     = intent.getStringExtra("Group");
        groupId       = intent.getIntExtra("GroupId", 0);
        companyId     = intent.getIntExtra("CompanyId", 0);

        getSupportActionBar().setTitle("Group " + groupName);

        String url    = ServiceUrl.USER_GROUP + "?company_id="+companyId+"&status=A&group_id="+groupId;
        new TaskCustomerGroup().execute(url);

        customersAdapter= new CustomersAdapter(this, customers);
        recyclerView    = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(customersAdapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);
    }

    private class TaskCustomerGroup extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(context);
            dialog.setMessage("Mohon tunggu...");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if(json != null) {
                try {
                    JSONArray response = json.getJSONArray("response");
                    int size = response.length();

                    if(size > 0) {
                        for (int i=0; i<size; i++) {
                            JSONObject customer = response.getJSONObject(i);
                            if(customer.getInt("group_id") == groupId) {
                                Customer Cust = new Customer();
                                Cust.setUserId(customer.getInt("user_id"));
                                Cust.setFirstName(customer.getString("firstname"));
                                Cust.setLastName(customer.getString("lastname"));
                                Cust.setEmail(customer.getString("email"));
                                Cust.setPhone(customer.getString("phone"));
                                Cust.setGroupName((customer.getString("group_name") == null ? "Retail" : customer.getString("group_name")));
                                Cust.setStatus(customer.getString("status"));
                                Cust.setBalance(customer.getDouble("balance"));
                                customers.add(Cust);
                                customersAdapter.notifyItemInserted(i);
                            }
                        }
                    }
                } catch (JSONException je) {
                    Log.e("Error", je.getLocalizedMessage());
                    Toast.makeText(context, je.getLocalizedMessage(), Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

}
