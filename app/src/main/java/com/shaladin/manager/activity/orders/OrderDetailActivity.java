package com.shaladin.manager.activity.orders;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mocoo.hang.rtprinter.driver.HsBluetoothPrintDriver;
import com.shaladin.manager.MyApps;
import com.shaladin.manager.R;
import com.shaladin.manager.adapter.ProductsAdapter;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Product;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class OrderDetailActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private static final int REQUEST_PERMISSION_READ_STORAGE    = 1001;
    private static final int REQUEST_PERMISSION_WRITE_STORAGE   = 1002;
    private RecyclerView recyclerView;
    public SharedPreferences UserAuth;
    private double subTotal, discount, total, shippingCost = 0;
    private HashMap<String, String> Billing  = new HashMap<>();
    private HashMap<String, String> Shipping = new HashMap<>();
    private HashMap<String, String> mapStatus= new HashMap<>();
    private HashMap<String, String> mapStatusCode= new HashMap<>();
    private List<String> Statuses = new ArrayList<>();
    private ArrayAdapter<String> statusAdapter;
    private ArrayList<Product> productsList = new ArrayList<>();
    private ProductsAdapter productsAdapter;
    private String statusCode, phoneNumber, emailAddress, notes, json;
    private BluetoothDevice bluetoothDevice;
    private int orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initStringMapping();
        UserAuth = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);

        //Check Permission Requirement
        initStoragePermission(1);

//        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        if(!bluetoothAdapter.isEnabled()) {
////            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
////            startActivity(enableBluetooth, null);
//            ((MyApps) getApplication()).enableBluetooth();
////            bluetoothDevice = ((MyApps) getApplication()).getConnection();
//        }
//        else
//        {
//            bluetoothDevice = ((MyApps) getApplication()).getConnection();
//        }

        Spinner sStatus = (Spinner) findViewById(R.id.spinner);
        statusAdapter   = new ArrayAdapter<String>(this, R.layout.spinner_item, Statuses);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sStatus.setAdapter(statusAdapter);
        sStatus.setOnItemSelectedListener( this );

        Intent intent = getIntent();
        orderId   = intent.getIntExtra("orderId", 0);

        getSupportActionBar().setTitle("Order #"+orderId);

        String orderDetailUri = ServiceUrl.ORDERS+"/"+orderId;
        new GetDetailTask().execute(orderDetailUri);

        recyclerView = (RecyclerView) findViewById(R.id.itemsProduct);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        productsAdapter = new ProductsAdapter(recyclerView.getContext(), productsList);
        recyclerView.setAdapter(productsAdapter);

        TextView shipAddress = (TextView) findViewById(R.id.headCustomer);
        shipAddress.setText("ALAMAT PENGIRIMAN");

        TextView itemsBought = (TextView) findViewById(R.id.titleItemsBrought);
        itemsBought.setText("PRODUK YANG DI PESAN");

        TextView paymentInfo = (TextView) findViewById(R.id.titlePaymentInstruction);
        paymentInfo.setText("INFORMASI PEMBAYARAN");

        Button actionSave = (Button) findViewById(R.id.btnSave);
        actionSave.setOnClickListener( this );
    }

    private void initStringMapping() {
        Statuses.add("Pilih Status");
        Statuses.add("Baru");
        Statuses.add("Selesai");
        Statuses.add("Dibatalkan");
        Statuses.add("Konfirmasi Pembayaran");
        Statuses.add("Proses");

        mapStatusCode.put("O", "Baru");
        mapStatusCode.put("C", "Selesai");
        mapStatusCode.put("I", "Dibatalkan");
        mapStatusCode.put("H", "Konfirmasi Pembayaran");
        mapStatusCode.put("P", "Proses Pembayaran");

        mapStatus.put("Baru", "O");
        mapStatus.put("Selesai", "C");
        mapStatus.put("Dibatalkan", "I");
        mapStatus.put("Konfirmasi Pembayaran", "H");
        mapStatus.put("Proses Pembayaran", "P");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner :
                statusCode  = mapStatus.get(parent.getItemAtPosition(position).toString());
                json = "{\"status\":\""+statusCode+"\", \"notes\":\"\"}";
                System.out.println(json);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave :
                String url = ServiceUrl.UPDATE_ORDERS + orderId;
                new UpdateOrderTask().execute(url);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.actionPrint :
//                doPrint();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void doPrint() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date now = new Date();

        HsBluetoothPrintDriver hsBluetoothPrintDriver = HsBluetoothPrintDriver.getInstance();
        hsBluetoothPrintDriver.Begin();
        hsBluetoothPrintDriver.SetDefaultSetting();
        hsBluetoothPrintDriver.SetAlignMode((byte)1);
        hsBluetoothPrintDriver.printString(UserAuth.getString("Company", "Store Manager"));
        hsBluetoothPrintDriver.printString(UserAuth.getString("Address", "Jl. Panjang No 9C Jakarta Barat"));
        hsBluetoothPrintDriver.printString("No Nota : " + orderId);
        hsBluetoothPrintDriver.printString(dateFormat.format(now));
        hsBluetoothPrintDriver.printString("--------------------------------");
        hsBluetoothPrintDriver.LF();
        hsBluetoothPrintDriver.CR();
        hsBluetoothPrintDriver.SetAlignMode((byte)0);

        if(productsList.size() > 0) {
            int size = productsList.size();
            for(int i = 0; i < size; i++) {
                Product product = productsList.get(i);
                if(i == 0){
                    hsBluetoothPrintDriver.printString(product.getProductName());
                    hsBluetoothPrintDriver.SetCharacterFont((byte)1);
                    hsBluetoothPrintDriver.BT_Write(product.getStock()+" x "+decimalFormat(product.getPrice()));
                    hsBluetoothPrintDriver.SetLeftStartSpacing((byte)2, (byte)1);
                    hsBluetoothPrintDriver.printString(decimalFormat(product.getStock()*product.getPrice()));
                }
                else {
                    hsBluetoothPrintDriver.SetLeftStartSpacing((byte)0, (byte)0);
                    hsBluetoothPrintDriver.printString(product.getProductName());
                    hsBluetoothPrintDriver.SetCharacterFont((byte)1);
                    hsBluetoothPrintDriver.BT_Write(product.getStock()+" x "+decimalFormat(product.getPrice()));
                    hsBluetoothPrintDriver.SetLeftStartSpacing((byte)2, (byte)1);
                    hsBluetoothPrintDriver.printString(decimalFormat(product.getStock()*product.getPrice()));
                }
                hsBluetoothPrintDriver.LF();
                hsBluetoothPrintDriver.CR();
            }
        }

        hsBluetoothPrintDriver.LF();
        hsBluetoothPrintDriver.CR();
        hsBluetoothPrintDriver.SetCharacterFont((byte)0);
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)0, (byte)0);
        hsBluetoothPrintDriver.printString("--------------------------------");
        hsBluetoothPrintDriver.SetAlignMode((byte)0);
        hsBluetoothPrintDriver.BT_Write("Sub Total");
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)200, (byte)0);
        hsBluetoothPrintDriver.BT_Write(" : ");
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)1, (byte)1);
        hsBluetoothPrintDriver.printString(decimalFormat(subTotal));
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)1, (byte)0);
        hsBluetoothPrintDriver.BT_Write("Pengiriman");
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)200, (byte)0);
        hsBluetoothPrintDriver.BT_Write(" : ");
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)1, (byte)1);
        hsBluetoothPrintDriver.printString(decimalFormat(shippingCost));
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)1, (byte)0);
        hsBluetoothPrintDriver.BT_Write("Total");
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)200, (byte)0);
        hsBluetoothPrintDriver.BT_Write(" : ");
        hsBluetoothPrintDriver.SetLeftStartSpacing((byte)1, (byte)1);
        hsBluetoothPrintDriver.printString(decimalFormat(total));
        hsBluetoothPrintDriver.CutPaper();

    }

    private class UpdateOrderTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(OrderDetailActivity.this);
            dialog.setTitle("Updating...");
            dialog.setMessage("Please wait we\'re update your order!");
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String response = HttpServices.postData(params[0], json);
                return response;
            } catch (Exception e) {
                Log.e("TAG", "Error: "+e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            dialog.dismiss();

            if(response != null) {
                Log.e("Response", response);
                Toast.makeText(getApplicationContext(), "Order has been updated!", Toast.LENGTH_LONG)
                        .show();
                //Back to Previous
                OrderDetailActivity.this.onBackPressed();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to update current order!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    private class GetDetailTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(OrderDetailActivity.this);
            dialog.setTitle("Please wait");
            dialog.setMessage("Getting detail order from server!");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try{
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception je) {
                Log.e("TAG", je.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if(json != null) {
                try {
                    JSONObject response = json.getJSONObject("response");
                    //Order Overview
                    subTotal = Double.parseDouble(response.getString("subtotal"));
                    shippingCost = Double.parseDouble(response.getString("shipping_cost"));
                    total = subTotal+shippingCost;
                    String _status = response.getString("status");
                    getSupportActionBar().setSubtitle(Html.fromHtml("<small>Status: "+mapStatusCode.get(_status)+"</small>"));

                    Spinner sStatus = (Spinner) findViewById(R.id.spinner);
                    sStatus.setSelection(statusAdapter.getPosition(mapStatusCode.get(_status)));
                    TextView iSubTotal = (TextView) findViewById(R.id.sumSubTotal);
                    TextView iShipCost = (TextView) findViewById(R.id.shippingCost);
                    TextView iTotal    = (TextView) findViewById(R.id.Total);
                    iSubTotal.setText(decimalFormat(subTotal));
                    iShipCost.setText(decimalFormat(shippingCost));
                    iTotal.setText(decimalFormat(total));

                    //Payment Method
                    JSONObject paymentMethod = response.getJSONObject("payment_method");
                    TextView paymentInstructions = (TextView) findViewById(R.id.instructions);
                    paymentInstructions.setText(Html.fromHtml(paymentMethod.getString("instructions")));

                    //Products
                    JSONObject products = response.getJSONObject("products");
                    Iterator x = products.keys();
                    JSONArray jsonArray = new JSONArray();

                    while(x.hasNext()) {
                        String key = (String) x.next();
                        jsonArray.put(products.get(key));
                    }

                    int productSize = jsonArray.length();
                    if(productSize > 0) {
                        for(int i=0; i<productSize; i++) {
                            Product _Product = new Product();

                            JSONObject product = jsonArray.getJSONObject(i);
                            String productName = product.getString("product");
                            int productId = Integer.parseInt(product.getString("product_id"));
                            double productPrice = Double.parseDouble(product.getString("price"));
                            int amount = Integer.parseInt(product.getString("amount"));
                            JSONObject mainPair= product.getJSONObject("main_pair");
                            JSONObject detailed= mainPair.getJSONObject("detailed");
                            String imagePath   = detailed.getString("image_path");

                            _Product.setId(i+1);
                            _Product.setProductId(productId);
                            _Product.setProductName(productName);
                            _Product.setViewed(0);
                            _Product.setPrice(productPrice);
                            _Product.setStock(amount);
                            _Product.setImage(imagePath);
                            _Product.setStatus("A");
                            productsList.add(_Product);
                            productsAdapter.notifyDataSetChanged();
                        }
                    }

                    //Shipping
                    JSONArray shipping   = response.getJSONArray("shipping");
                    JSONObject _shipping = shipping.getJSONObject(0);
                    TextView ShippingDesc= (TextView) findViewById(R.id.shipping);
                    ShippingDesc.setText(_shipping.getString("shipping") + " ("+_shipping.getString("delivery_time")+" hari)");

                    //Billing Address
                    Billing.put("firstname", response.getString("b_firstname"));
                    Billing.put("lastname", response.getString("b_lastname"));
                    Billing.put("address", response.getString("b_address"));
                    Billing.put("phone", response.getString("b_phone"));
                    setBillingAddress(Billing);

                } catch (JSONException je) {
                    Log.e("TAG", "Error: "+je.getLocalizedMessage());
                }
            } else {
                Toast.makeText(getApplicationContext(), "Oops, terjadi kesalahan! Silakan coba lagi.", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    private String decimalFormat(double number) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(number);
    }

    private void setBillingAddress(HashMap<String, String> Billing) {
        TextView customerName = (TextView) findViewById(R.id.accountName);
        TextView customerAddress = (TextView) findViewById(R.id.accountAddress);

        customerName.setText(Billing.get("firstname")+" "+Billing.get("lastname"));
        customerAddress.setText(Html.fromHtml(Billing.get("address")+"<br/> HP: <strong>"+Billing.get("phone")+"</strong>"));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initStoragePermission(int type) {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//                != PackageManager.PERMISSION_GRANTED) {
//            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
//                Toast.makeText(this, "Permission to use Camera", Toast.LENGTH_SHORT).show();
//            }
//            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
//        } else {
//            initCameraIntent();
//        }
        if(type == 1) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if(shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Permission to Read Storage", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_READ_STORAGE);
                }
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_READ_STORAGE);
            }
        }
        else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Permission to Write Storage", Toast.LENGTH_LONG).show();
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_STORAGE);
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_READ_STORAGE) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initStoragePermission(2);
                Toast.makeText(this, "Permission granted.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Permission denied.", Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == REQUEST_PERMISSION_WRITE_STORAGE) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(this, "Permission Read Storage Denied", Toast.LENGTH_LONG).show();
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
