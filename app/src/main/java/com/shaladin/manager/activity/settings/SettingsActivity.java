package com.shaladin.manager.activity.settings;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.CustomGridViewAdapter;

public class SettingsActivity extends AppCompatActivity {
    CoordinatorLayout rootLayout;
    GridView gridView;

    public static String[] gridViewStrings = {
            "Profile",
            "Toko",
            "Pembayaran",
            "Nota",
            "Ganti Password",
            "Printer"
    };

    public static int[] gridViewImages = {
            R.drawable.ic_account,
            R.drawable.ic_store_2,
            R.drawable.ic_bank,
            R.drawable.ic_receipt,
            R.drawable.ic_unlocked,
            R.drawable.ic_printer
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rootLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator);
        gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new CustomGridViewAdapter(this, gridViewStrings, gridViewImages));
    }

}
