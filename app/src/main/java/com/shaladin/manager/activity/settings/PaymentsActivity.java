package com.shaladin.manager.activity.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.PaymentsAdapter;
import com.shaladin.manager.collection.PaymentCollection;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Payment;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PaymentsActivity extends AppCompatActivity {
    private String NamaBank;
    private String NoRekening;
    private String NamaPemilik;
    private int companyId;
    private String serviceUrl = ServiceUrl.COMPANY_PAYMENT;
    private Payment payment;
    private List<Payment> payments;
    private PaymentCollection paymentCollection;
    private PaymentsAdapter paymentAdapter;
    private Context context;

    SharedPreferences AuthUser;
    RecyclerView recycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(getString(R.string.title_activity_settings));
        getSupportActionBar().setSubtitle(getString(R.string.title_activity_payments));

        context  = this;
        AuthUser = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        companyId= AuthUser.getInt("CompanyId", 0);

        paymentCollection = new PaymentCollection(context);
        paymentCollection.open();
        System.out.println(paymentCollection.count(companyId));
        payments = paymentCollection.all(companyId);
        if(payments.size() == 0) {
            payments = new ArrayList<>();
            showDialogAddPayment();
        }

        paymentAdapter = new PaymentsAdapter(context, payments);
        recycleView = (RecyclerView) findViewById(R.id.recyclerView);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        recycleView.setHasFixedSize(true);
        recycleView.setAdapter(paymentAdapter);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(recycleView.getContext(), DividerItemDecoration.VERTICAL_LIST);
        recycleView.addItemDecoration(itemDecoration);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddPayment();
            }
        });
    }

    private void showDialogAddPayment() {
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Tambah Rekening Bank")
                .setView(R.layout.dialog_add_payment)
                .create();
        dialog.show();
        Button actionSaveNew = (Button) dialog.findViewById(R.id.actionSave);
        actionSaveNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View focusView;
                EditText bankProvider   = (EditText) dialog.findViewById(R.id.bank);
                EditText noRekening     = (EditText) dialog.findViewById(R.id.noRekening);
                EditText pemilikRekening= (EditText) dialog.findViewById(R.id.namaPemilik);

                if(TextUtils.isEmpty(bankProvider.getText().toString())) {
                    bankProvider.setError("Nama Bank harus di isi!");
                    focusView = bankProvider;
                    focusView.requestFocus();
                }
                else if(TextUtils.isEmpty(noRekening.getText().toString())) {
                    noRekening.setError("No Rekening harus di isi!");
                    focusView = noRekening;
                    focusView.requestFocus();
                }
                else if(TextUtils.isEmpty(pemilikRekening.getText().toString())) {
                    pemilikRekening.setError("Nama Pemilik Rekening harus di isi!");
                    focusView = pemilikRekening;
                    focusView.requestFocus();
                }
                else {
                    NamaBank = bankProvider.getText().toString();
                    NoRekening = noRekening.getText().toString();
                    NamaPemilik= pemilikRekening.getText().toString();
                    String instructions = "<p>An :  "+ NamaPemilik +"<br><span style=\"font-size: small;\" rel=\"font-size: small;\"><strong>Nomor : "+ NoRekening +"</strong></span><br>"+ NamaBank +"<br><br></p>";
                    try {
                        JSONObject json = new JSONObject();
                        json.put("company_id", companyId);
                        json.put("payment_name", "Transfer "+NamaBank);
                        json.put("description", "A/N: "+NamaPemilik+" No: "+NoRekening);
                        json.put("instructions", instructions);
                        json.put("status", "H");

                        payment = new Payment();
                        payment.setCompanyId(companyId);
                        payment.setPaymentName(json.getString("payment_name"));
                        payment.setDescription(json.getString("description"));
                        payment.setInstructions(json.getString("instructions"));
                        payment.setStatus(json.getString("status"));

//                        System.out.println(json.toString());
                        new SubmitPaymentTask().execute(serviceUrl, json.toString());
                    } catch (JSONException je) {
                        Log.i(getString(R.string.app_name), je.getLocalizedMessage());
                    }
//                    onResult();
                    dialog.dismiss();
                }

            }
        });
    }

    private void onResult() {
        System.out.println(NamaBank+NoRekening+NamaPemilik);
    }

    private class SubmitPaymentTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(PaymentsActivity.this);
            dialog.setTitle("Please wait...");
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String result = HttpServices.postData(params[0], params[1]);
                return result;
            } catch (Exception e) {
                Log.i(getString(R.string.app_name), e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            if(result != null) {
                try {
                    JSONObject r = new JSONObject(result);
                    boolean isError = r.getBoolean("error");

                    if(!isError) {
                        paymentCollection = new PaymentCollection(context);
                        paymentCollection.open();
                        paymentCollection.create(payment);
                        System.out.println(paymentCollection.count(companyId));
                        paymentCollection.close();

                        payments.add(payment);
                        paymentAdapter.notifyDataSetChanged();
                        Toast.makeText(PaymentsActivity.this, "Rekening Bank berhasil di tambahkan!", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (JSONException je) {
                    Log.i(getString(R.string.app_name), je.getLocalizedMessage());
                }
            }
            else {
                Toast.makeText(PaymentsActivity.this, "Gagal Menyimpan, silakan coba kembali!", Toast.LENGTH_LONG)
                        .show();
            }
        }

    }

    public static class MyDialogFragment extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.dialog_add_payment, container, false);
            getDialog().setTitle("Tambah Rekening Bank");
            return rootView;
        }
    }

}
