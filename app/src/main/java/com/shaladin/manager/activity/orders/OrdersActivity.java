package com.shaladin.manager.activity.orders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.OrdersAdapter;
import com.shaladin.manager.collection.StatusCollection;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Order;
import com.shaladin.manager.model.Status;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrdersActivity extends AppCompatActivity {

    private List<Order> orders = new ArrayList<>();
    private List<Status> statuses;
    private OrdersAdapter adapter;
    private HashMap<String, String> StatusMap;
    private final String AuthPrefs = "AuthPrefs";

    //initialize authSharedPreferences
//    SharedPreferences AuthUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences UserAuth = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        getSupportActionBar().setTitle(UserAuth.getString("Company", getString(R.string.app_name)));
        getSupportActionBar().setSubtitle("Pesanan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        int CompanyId = UserAuth.getInt("CompanyId", 0);
        String uri = ServiceUrl.ORDERS + "?company_id=" + CompanyId;
        new OrderTask().execute(uri);

        adapter = new OrdersAdapter(this, orders);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        StatusCollection statusCollection = new StatusCollection(this);
        statusCollection.open();
        statuses = statusCollection.all();
        statusCollection.close();

        if(statuses.size() > 0) {
            StatusMap = new HashMap<>();
            for (Status status : statuses) {
                StatusMap.put(status.getStatus(), status.getDescription());
            }
        }

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    private class OrderTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(OrdersActivity.this);
            dialog.setTitle("Please wait...");
            dialog.setMessage("Fetching data from server!");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject jsonObject = HttpServices.getData(params[0]);
                if(jsonObject != null) {
                    return jsonObject;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            dialog.dismiss();

            if(jsonObject != null) {
                try {
                    if (jsonObject.has("error") && !jsonObject.getBoolean("error")) {
                        JSONObject response = jsonObject.getJSONObject("response");
                        JSONArray arrOrders = response.getJSONArray("orders");
                        int size = arrOrders.length();

                        if(size > 0) {
                            for (int i=0; i<size; i++) {
                                JSONObject order = arrOrders.getJSONObject(i);

                                long modelID= Long.parseLong(order.getString("order_id"));
                                int orderId = Integer.parseInt(order.getString("order_id"));
                                int userId  = Integer.parseInt(order.getString("user_id"));

                                Order model = new Order();
                                model.setId(modelID);
                                model.setOrderId(orderId);
                                model.setUserId(userId);
                                model.setbFirstname(order.getString("firstname"));
                                model.setbLastname(order.getString("lastname"));
                                model.setbPhone(order.getString("phone"));
//                                model.set(order.getString("email"));
                                model.setbCity(order.getString("s_kabupaten_name"));
                                model.setbDistrict(order.getString("s_city_name"));
                                model.setTotal(Double.parseDouble(order.getString("total")));
                                model.setTimestamp(Integer.parseInt(order.getString("timestamp")));
                                model.setStatus(StatusMap.get(order.getString("status")));

                                orders.add(model);
                                adapter.notifyItemInserted(i);
                            }
                        }
                    }
                } catch (JSONException je) {
                    Log.e("TAG", je.getLocalizedMessage());
                }
            }
        }
    }

}
