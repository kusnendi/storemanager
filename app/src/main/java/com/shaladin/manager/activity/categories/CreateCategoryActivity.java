package com.shaladin.manager.activity.categories;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.shaladin.manager.R;
import com.shaladin.manager.collection.CategoryCollection;
import com.shaladin.manager.model.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CreateCategoryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    List<Category> ParentCategories = new ArrayList<>();
    HashMap<String, Integer> CategoriesMap;
    private CategoryCollection categoryCollection;
    private Category kategori;
    private int categoryId;
    private String description;
    private EditText iDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initBaseCategories();
        String[] _categories = {
                "Pilih Parent",
                "Fashion Wanita",
                "Fashion Anak",
                "Busana Muslim",
                "Busana Pria",
                "Batik",
                "Tas dan Sepatu",
                "Interior",
                "Aksesories",
                "Baju Tidur",
                "Pakaian Olahraga",
                "Pakaian Dalam"
        };

        Spinner categories = (Spinner) findViewById(R.id.categories);
        ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, _categories);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categories.setAdapter(categoriesAdapter);
        categories.setOnItemSelectedListener( this );

        Button actionSave = (Button) findViewById(R.id.actionSave);
        actionSave.setOnClickListener( this );
    }

    private void initBaseCategories() {
        String[] _categories = {
                "Pilih Parent",
                "Fashion Wanita",
                "Fashion Anak",
                "Busana Muslim",
                "Busana Pria",
                "Batik",
                "Tas dan Sepatu",
                "Interior",
                "Aksesories",
                "Baju Tidur",
                "Pakaian Olahraga",
                "Pakaian Dalam"
        };

        int[] _ids = {0, 165, 166, 167, 168, 171, 175, 177, 178, 284, 289, 290};

        int j = 1;
        for (int i=0; i<_categories.length; i++) {
            Category category = new Category();
            category.setId(j);
            category.setParentId(0);
            category.setCategoryId(_ids[i]);
            category.setDescription(_categories[i]);
            j++;

            ParentCategories.add(category);
        }

        if(ParentCategories.size() > 0) {
            CategoriesMap = new HashMap<>();
            for(Category category : ParentCategories) {
                CategoriesMap.put(category.getDescription(), category.getCategoryId());
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.categories :
                categoryId = CategoriesMap.get(parent.getItemAtPosition(position).toString());
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionSave :
                iDescription = (EditText) findViewById(R.id.description);
                description  = iDescription.getText().toString();

                Category category = new Category();
                category.setParentId(categoryId);
                category.setCategoryId(categoryId);
                category.setDescription(description);

                if(categoryId == 0) {
                    //
                } else if(TextUtils.isEmpty(description)) {
                    //
                }
                else {
                    categoryCollection = new CategoryCollection(this);
                    categoryCollection.open();
                    categoryCollection.create(category);

                    Intent intent = new Intent(this, CategoriesActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(intent);
                }
                break;
        }
    }
}
