package com.shaladin.manager.activity.orders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.OrdersAdapter;
import com.shaladin.manager.collection.StatusCollection;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Order;
import com.shaladin.manager.model.Status;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderHistoryActivity extends AppCompatActivity {

    private List<Order> orders = new ArrayList<>();
    private List<Status> statuses;
    private OrdersAdapter adapter;
    private HashMap<String, String> StatusMap;
    private final String AuthPrefs = "AuthPrefs";

    ImageView imageException;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        int userId    = intent.getIntExtra("userId", 0);

        SharedPreferences UserAuth = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        int CompanyId = UserAuth.getInt("CompanyId", 0);
        String uri = ServiceUrl.ORDERS + "?company_id=" + CompanyId + "&user_id=" + userId;
        new OrderTask().execute(uri);

        imageException = (ImageView) findViewById(R.id.imageException);
        System.out.println(imageException.getVisibility());

        adapter = new OrdersAdapter(this, orders);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        StatusCollection statusCollection = new StatusCollection(this);
        statusCollection.open();
        statuses = statusCollection.all();
        statusCollection.close();

        if(statuses.size() > 0) {
            StatusMap = new HashMap<>();
            for (Status status : statuses) {
                StatusMap.put(status.getStatus(), status.getDescription());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home :
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private class OrderTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(OrderHistoryActivity.this);
            dialog.setTitle("Please wait...");
            dialog.setMessage("Fetching data from server!");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject jsonObject = HttpServices.getData(params[0]);
                if(jsonObject != null) {
                    return jsonObject;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            dialog.dismiss();

            if(jsonObject != null) {
                try {
                    boolean error = jsonObject.getBoolean("error");
                    if (!error) {
                        JSONObject response = jsonObject.getJSONObject("response");
                        JSONArray arrOrders = response.getJSONArray("orders");
                        int size = arrOrders.length();

                        if(size > 0) {
                            for (int i=0; i<size; i++) {
                                JSONObject order = arrOrders.getJSONObject(i);

                                long modelID= Long.parseLong(order.getString("order_id"));
                                int orderId = Integer.parseInt(order.getString("order_id"));
                                int userId  = Integer.parseInt(order.getString("user_id"));

                                Order model = new Order();
                                model.setId(modelID);
                                model.setOrderId(orderId);
                                model.setUserId(userId);
                                model.setbFirstname(order.getString("firstname"));
                                model.setbLastname(order.getString("lastname"));
                                model.setbPhone(order.getString("phone"));
//                                model.set(order.getString("email"));
                                model.setbCity(order.getString("s_kabupaten_name"));
                                model.setbDistrict(order.getString("s_city_name"));
                                model.setTotal(Double.parseDouble(order.getString("total")));
                                model.setTimestamp(Integer.parseInt(order.getString("timestamp")));
                                model.setStatus(StatusMap.get(order.getString("status")));

                                orders.add(model);
                                adapter.notifyItemInserted(i);
                            }
                        }
                    }
                    else {
                        if(imageException.getVisibility() == View.GONE) {
                            imageException.setVisibility(View.VISIBLE);
                        }
                        Toast.makeText(OrderHistoryActivity.this, "Belum ada riwayat pesanan!", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (JSONException je) {
                    Log.e("TAG", je.getLocalizedMessage());
                }
            }
        }
    }

}
