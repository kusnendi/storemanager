package com.shaladin.manager.activity.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private int companyId;

    SharedPreferences profilPrefs;
    SharedPreferences userAuth;
    EditText firstname;
    EditText lastname;
    EditText address;
    EditText email;
    EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_activity_settings));
        getSupportActionBar().setSubtitle(getString(R.string.title_activity_profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        userAuth   = getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        companyId  = userAuth.getInt("UserId", 0);

        profilPrefs= getSharedPreferences("UserProfile", Context.MODE_PRIVATE);
        String serviceProfile = ServiceUrl.USER + "/" + companyId;
        new UserProfilTask().execute(serviceProfile);
        init();
    }

    private void init(){
        firstname = (EditText) findViewById(R.id.firstname);
        lastname  = (EditText) findViewById(R.id.lastname);
        address   = (EditText) findViewById(R.id.address);
        email     = (EditText) findViewById(R.id.email);
        phone     = (EditText) findViewById(R.id.phone);

        firstname.setText(profilPrefs.getString("FirstName", ""));
        lastname.setText(profilPrefs.getString("LastName", ""));
        address.setText(profilPrefs.getString("Address",""));
        email.setText(profilPrefs.getString("Email",""));
        phone.setText(profilPrefs.getString("Phone",""));

        Button actionSave = (Button) findViewById(R.id.actionSave);
        actionSave.setOnClickListener( this );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionSave :
                if(TextUtils.isEmpty(firstname.getText().toString())) {
                    firstname.requestFocus();
                    firstname.setError("Nama Depan wajib di isi!");
                } else if (TextUtils.isEmpty(lastname.getText().toString())) {
                    lastname.requestFocus();
                    lastname.setError("Nama Belakang wajib di isi!");
                } else if (TextUtils.isEmpty(address.getText().toString())) {
                    address.requestFocus();
                    address.setError("Alamat harus di isi!");
                } else if (TextUtils.isEmpty(email.getText().toString())) {
                    email.requestFocus();
                    email.setError("Email harus di isi!");
                } else if (TextUtils.isEmpty(phone.getText().toString())) {
                    phone.requestFocus();
                    phone.setError("HP/Telepon Wajib di isi!");
                } else {

                    SharedPreferences.Editor editor = profilPrefs.edit();
                    editor.putString("FirstName", firstname.getText().toString());
                    editor.putString("LastName", lastname.getText().toString());
                    editor.putString("Address", address.getText().toString());
                    editor.putString("Email", email.getText().toString());
                    editor.putString("Phone", phone.getText().toString());
                    editor.commit();

                    try {

                        JSONObject json = new JSONObject();
                        json.put("firstname", firstname.getText().toString());
                        json.put("lastname", lastname.getText().toString());
                        json.put("alamat", address.getText().toString());
                        json.put("email", email.getText().toString());
                        json.put("telepon", phone.getText().toString());

                        String url = ServiceUrl.USER_PROFILE + companyId;
                        new UpdateProfileTask().execute(url, json.toString());

                    } catch (JSONException je) {
                        Log.i("JSON", je.getLocalizedMessage());
                    }

                    Toast.makeText(this, "Data berhasil di perbarui!", Toast.LENGTH_LONG)
                            .show();
                }
                break;
        }
    }

    private class UpdateProfileTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(ProfileActivity.this);
            dialog.setTitle("Please wait...");
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String result = HttpServices.postData(params[0], params[1]);
                return result;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            if(result != null) {
                System.out.println(result);
            }
        }
    }

    private class UserProfilTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception e) {
                Log.i(getString(R.string.app_name), e.getLocalizedMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            if(json != null) {
                try {
                    JSONObject data = json.getJSONObject("response");
                    SharedPreferences.Editor editProfile = profilPrefs.edit();
                    editProfile.putString("FirstName", data.getString("firstname"));
                    editProfile.putString("LastName", data.getString("lastname"));
                    editProfile.putString("Address", data.getString("address"));
                    editProfile.putString("Email", data.getString("email"));
                    editProfile.putString("Phone", data.getString("phone"));
                    editProfile.commit();

                    init();
                } catch (JSONException je) {
                    Log.i(getString(R.string.app_name), je.getLocalizedMessage());
                }
            }
            else {
                Toast.makeText(ProfileActivity.this, "Maaf terjadi kesalahan, sialakan coba kembali!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
