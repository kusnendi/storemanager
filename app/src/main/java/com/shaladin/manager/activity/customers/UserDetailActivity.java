package com.shaladin.manager.activity.customers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.activity.orders.OrderHistoryActivity;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Customer;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

public class UserDetailActivity extends AppCompatActivity {

    private final String AuthPrefs = "AuthPrefs";
    private final String CompanyId = "CompanyId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        Bundle data = intent.getBundleExtra("data");
        final Customer customer = (Customer) data.getSerializable("customer");

        new CustomerDetailTask().execute(ServiceUrl.USER + "/" + customer.getUserId());

        SharedPreferences AuthPref = getSharedPreferences(AuthPrefs, Context.MODE_PRIVATE);
        final int companyId = AuthPref.getInt(CompanyId, 0);

        getSupportActionBar().setTitle(customer.getFirstName()+" "+customer.getLastName());
        TextView email = (TextView) findViewById(R.id.emailUser);
        email.setText(customer.getEmail());

        TextView phone = (TextView) findViewById(R.id.phonelUser);
        phone.setText(customer.getPhone());

        Button orderHistory = (Button) findViewById(R.id.orderHistory);
        orderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(view.getContext(), OrderHistoryActivity.class);
                intent1.putExtra("userId", customer.getUserId());
                startActivity(intent1);
            }
        });

        Button disableUser  = (Button) findViewById(R.id.disableUser);
        disableUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(UserDetailActivity.this)
                    .setTitle("Confirmation!")
                    .setMessage("Apa anda yakin ingin menonaktifkan user ini?")
                    .setNegativeButton("Tidak", null)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                JSONObject data = new JSONObject();
                                data.put("user_id", customer.getUserId());
                                data.put("company_id", companyId);
                                data.put("status", "D");

                                String url = ServiceUrl.STATUS_MEMBERS;
                                new DisableUserTask().execute(url, data.toString());
//                                System.out.println(data.toString());
                            } catch (JSONException je) {
                                Log.i("Error", je.getLocalizedMessage());
                            }
                        }
                    })
                    .create();

                dialog.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home :
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DisableUserTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                String result = HttpServices.postData(params[0], params[1]);
                return result;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(result != null) {
                System.out.println(result);
            } else {
                System.out.println("Something when wrong!");
            }
        }
    }

    private class CustomerDetailTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(UserDetailActivity.this);
            dialog.setMessage("Please wait...");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if(json != null) {
                System.out.println(json.toString());
                try {
                    boolean error = json.getBoolean("error");

                    if(!error) {
                        JSONObject response = json.getJSONObject("response");
                        String alamat = (response.getString("address").isEmpty() ? "-" :  response.getString("address"));

                        TextView address = (TextView) findViewById(R.id.addressUser);
                        address.setText(alamat);
                    }
                } catch (JSONException je) {
                    Log.i("Error", je.getLocalizedMessage());
                }
            }
            else {
                Toast.makeText(UserDetailActivity.this, "Terjadi kesalahan, silakan coba kembali", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
