package com.shaladin.manager.activity.customers;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.shaladin.manager.adapter.CustomGridViewAdapter;
import com.shaladin.manager.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserGroupFragment extends Fragment {

    GridView gridView;

    public static String[] gridViewStrings = {
            "Retail",
            "Reseller",
            "Distributor"
    };

    public static int[] gridViewImages = {
            R.drawable.ic_retail,
            R.drawable.ic_reseller,
            R.drawable.ic_distributor
    };

    public UserGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.content_gridview, container, false);

        gridView = (GridView) view.findViewById(R.id.grid);
        gridView.setAdapter(new CustomGridViewAdapter(getActivity(), gridViewStrings, gridViewImages));
        return view;
    }

}
