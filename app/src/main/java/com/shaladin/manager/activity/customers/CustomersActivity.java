package com.shaladin.manager.activity.customers;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.GridView;

import com.shaladin.manager.R;

import java.util.ArrayList;
import java.util.List;

public class CustomersActivity extends AppCompatActivity {

    public static final int Retail      = 19;
    public static final int Reseller    = 18;
    public static final int Distributor = 17;

    CoordinatorLayout rootLayout;
    GridView gridView;

    public static String[] gridViewStrings = {
            "Retail",
            "Reseller",
            "Distributor"
    };

    public static int[] gridViewImages = {
            R.drawable.ic_retail,
            R.drawable.ic_reseller,
            R.drawable.ic_distributor
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(viewPager);

        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

//        rootLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator);
//        gridView = (GridView) findViewById(R.id.grid);
//        gridView.setAdapter(new CustomGridViewAdapter(this, gridViewStrings, gridViewImages));

    }

    private void setupViewPager(ViewPager viewPager) {
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager());
        tabsAdapter.addFragment(new UserRequestsFragment(), "Request");
        tabsAdapter.addFragment(new UserGroupFragment(), "Group");
        viewPager.setAdapter(tabsAdapter);
    }

    static class TabsAdapter extends FragmentPagerAdapter {
        List<Fragment> mFragments = new ArrayList<>();
        List<String> mTitles = new ArrayList<>();

        TabsAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mTitles.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

}
