package com.shaladin.manager.activity.products;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.activity.customers.UserGroupFragment;
import com.shaladin.manager.activity.customers.UserRequestsFragment;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.utils.HttpServices;

import java.util.ArrayList;
import java.util.List;

public class EditProductActivity extends AppCompatActivity {
    public final String EXTRA_PRODUCT_ID = "product_id";
    public final String EXTRA_PRODUCT = "product";
    public final String EXTRA_AMOUNT  = "amount";
    public final String EXTRA_IMAGE   = "image";
    public final String EXTRA_PRICE   = "price";
    public final String EXTRA_VIEWED  = "viewed";

    private int productId;
    private int productAmount;
    private int productPrice;
    private String productName;
    private String productDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        productId = intent.getIntExtra(EXTRA_PRODUCT_ID, 0);
        productAmount = intent.getIntExtra(EXTRA_AMOUNT, 0);
        productName = intent.getStringExtra(EXTRA_PRODUCT);
        productDesc = intent.getStringExtra("detail");
        productPrice = intent.getIntExtra(EXTRA_PRICE, 0);

        getSupportActionBar().setTitle(productName);
//        getSupportActionBar().setSubtitle("Edit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(viewPager);

        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

//        final EditText product  = (EditText) findViewById(R.id.product);
//        product.setText(productName);
//        final EditText price    = (EditText) findViewById(R.id.price);
//        price.setText(String.valueOf(productPrice));
//        final EditText stock    = (EditText) findViewById(R.id.stock);
//        stock.setText(String.valueOf(productAmount));
//        stock.requestFocus();
//        final EditText description = (EditText) findViewById(R.id.description);
//        description.setText(Html.fromHtml(productDesc));
//
//        Button actionUpdate = (Button) findViewById(R.id.actionUpdate);
//        actionUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String url = ServiceUrl.PRODUCTS + "/" + productId;
//                productName  = product.getText().toString();
//                productPrice = Integer.parseInt(price.getText().toString());
//                productAmount= Integer.parseInt(stock.getText().toString());
//                productDesc  = description.getText().toString();
//
//                new UpdateProductTask().execute(url, jsonProduct(productName, productPrice, productAmount, productDesc));
////                System.out.println(jsonProduct(productName, productPrice, productAmount, productDesc));
//            }
//        });
    }

    private void setupViewPager(ViewPager viewPager) {
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager());
        tabsAdapter.addFragment(new GeneralFragment(), "Informasi");
        tabsAdapter.addFragment(new PricesFragment(), "List Harga");
        viewPager.setAdapter(tabsAdapter);
    }

    static class TabsAdapter extends FragmentPagerAdapter {
        List<Fragment> mFragments = new ArrayList<>();
        List<String> mTitles = new ArrayList<>();

        TabsAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mTitles.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

    private String jsonProduct(String product, int price, int amount, String description) {
        String json = "{\"product\":\""+product+"\"," +
                "\"price\":\""+price+"\", " +
                "\"amount\":\""+amount+"\", " +
                "\"full_description\":\""+description.replaceAll("[\\n]", "<br/>").replaceAll("\"","'")+"\"}";

        return json;
    }

    private class UpdateProductTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getApplicationContext());
            dialog.setTitle("Please Wait");
            dialog.setMessage("Mohon tunggu, proses menyimpan product ke server.");
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String response = HttpServices.putData(params[0], params[1]);
                return response;
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            dialog.dismiss();

            if(response != null) {
                Log.e("Response", response);
                Toast.makeText(getApplicationContext(), "Produk baru berhasil di update!", Toast.LENGTH_LONG)
                        .show();
                EditProductActivity.this.finish();
            } else {
                Log.e("Error:", "Request Error!");
            }
        }
    }

}
