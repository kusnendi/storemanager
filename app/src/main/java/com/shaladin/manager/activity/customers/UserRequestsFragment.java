package com.shaladin.manager.activity.customers;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.adapter.CustomerRequestAdapter;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.model.Customer;
import com.shaladin.manager.model.User;
import com.shaladin.manager.utils.DividerItemDecoration;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserRequestsFragment extends Fragment {
    private List<Customer> lists = new ArrayList<>();
    private CustomerRequestAdapter requestAdapter;
    private int companyId;

    RecyclerView recyclerView;
    SharedPreferences UserAuth;

    public UserRequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_requests, container, false);
        UserAuth = getActivity().getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);
        companyId= UserAuth.getInt("CompanyId", 0);

        String serviceUrl = ServiceUrl.USER_GROUP + "?company_id=" + companyId + "&items_per_page=50&status=D";
        new TaskUserRequests().execute(serviceUrl);

        requestAdapter  = new CustomerRequestAdapter(getContext(), lists);
        recyclerView    = (RecyclerView) view.findViewById(R.id.recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(requestAdapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);
        return view;
    }

    private class TaskUserRequests extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Please wait...");
            dialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = HttpServices.getData(params[0]);
                return json;
            } catch (Exception e) {
                Log.i(getString(R.string.app_name), e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if(json != null) {
                try {
                    JSONArray customers = json.getJSONArray("response");
                    int size = customers.length();

                    if(size > 0) {
                        for (int i=0; i<size; i++) {
                            Customer Cust = new Customer();
                            JSONObject customer = customers.getJSONObject(i);

                            Cust.setUserId(customer.getInt("user_id"));
                            Cust.setFirstName(customer.getString("firstname"));
                            Cust.setLastName(customer.getString("lastname"));
                            Cust.setEmail(customer.getString("email"));
//                            if(customer.getString("group_id") == null) Cust.setGroupId(19);
//                            else Cust.setGroupId(Integer.parseInt(customer.getString("group_id")));
//                            Cust.setGroupId((customer.getString("group_id") != null ? Integer.parseInt(customer.getString("group_id")) : 19));
                            Cust.setGroupName((customer.getString("group_name") == null ? "Retail" : customer.getString("group_name")));
                            Cust.setStatus(customer.getString("status"));

                            lists.add(Cust);
                            requestAdapter.notifyItemInserted(i);
                        }
                    }
                } catch (JSONException je) {
                    Log.i(getString(R.string.app_name), je.getLocalizedMessage());
                }
            } else {
                Toast.makeText(getContext(), "Oops, Silakan coba kembali!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}
