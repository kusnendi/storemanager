package com.shaladin.manager.activity.products;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.utils.HttpServices;
import com.shaladin.manager.utils.ImageUtils;
import com.shaladin.manager.utils.RealPathUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralFragment extends Fragment {

    public final String EXTRA_PRODUCT_ID = "product_id";
    public final String EXTRA_PRODUCT = "product";
    public final String EXTRA_AMOUNT  = "amount";
    public final String EXTRA_IMAGE   = "image";
    public final String EXTRA_PRICE   = "price";
    public final String EXTRA_VIEWED  = "viewed";

    private int productId;
    private int productAmount;
    private int productPrice;
    private String productName;
    private String productDesc;

    private static final int REQUEST_CAMERA = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;
    private static final String http_image_demo = "http://www.grosirbersama.co.id/images/detailed/31/1359-250__blouse_titanium.jpg";
    private final CharSequence[] items = {"Take Photo", "From Gallery"};

    public SharedPreferences UserAuth;
    private Bitmap mBitmap;
    private ImageView iv_image;
    private Uri selectedImageUri;
    private String realPath   = "";
    private String http_image = "";

    View view;
    public GeneralFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_product_information, container, false);

        Intent intent   = getActivity().getIntent();
        productId       = intent.getIntExtra(EXTRA_PRODUCT_ID, 0);
        productAmount   = intent.getIntExtra(EXTRA_AMOUNT, 0);
        productName     = intent.getStringExtra(EXTRA_PRODUCT);
        productDesc     = intent.getStringExtra("detail");
        productPrice    = intent.getIntExtra(EXTRA_PRICE, 0);
        String Image    = intent.getStringExtra("productImage");

        final EditText product  = (EditText) view.findViewById(R.id.product);
        product.setText(productName);
        final EditText price    = (EditText) view.findViewById(R.id.price);
        price.setText(String.valueOf(productPrice));
        final EditText stock    = (EditText) view.findViewById(R.id.stock);
        stock.setText(String.valueOf(productAmount));
        stock.requestFocus();
        final EditText description = (EditText) view.findViewById(R.id.description);
        description.setText(Html.fromHtml(productDesc));

        Button actionUpdate = (Button) view.findViewById(R.id.actionUpdate);
        actionUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = ServiceUrl.PRODUCTS + "/" + productId;
                productName  = product.getText().toString();
                productPrice = Integer.parseInt(price.getText().toString());
                productAmount= Integer.parseInt(stock.getText().toString());
                productDesc  = description.getText().toString();

                new UpdateProductTask().execute(url, jsonProduct(productName, productPrice, productAmount, productDesc));
//                System.out.println(jsonProduct(productName, productPrice, productAmount, productDesc));
            }
        });
        Picasso.with(getContext()).load(Image).placeholder(R.drawable.placeholder)
                .into((ImageView) view.findViewById(R.id.addImage));

        ImageView addImage = (ImageView) view.findViewById(R.id.addImage);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooserDialog();
            }
        });

        return view;
    }

    private String jsonProduct(String product, int price, int amount, String description) {
        String json;

        if(!http_image.isEmpty()) {
            json = "{\"product\":\""+product+"\"," +
                    "\"price\":\""+price+"\", " +
                    "\"amount\":\""+amount+"\", " +
                    "\"image\":\""+http_image+"\", " +
                    "\"full_description\":\""+description.replaceAll("[\\n]", "<br/>").replaceAll("\"","'")+"\"}";
        } else {
            json = "{\"product\":\""+product+"\"," +
                    "\"price\":\""+price+"\", " +
                    "\"amount\":\""+amount+"\", " +
                    "\"full_description\":\""+description.replaceAll("[\\n]", "<br/>").replaceAll("\"","'")+"\"}";
        }

        return json;
    }

    private class UpdateProductTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Mohon tunggu, proses menyimpan product ke server.");
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String response = HttpServices.putData(params[0], params[1]);
                return response;
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            dialog.dismiss();

            if(response != null) {
                Log.e("Response", response);
                Toast.makeText(getContext(), "Produk baru berhasil di update!", Toast.LENGTH_LONG)
                        .show();
                getActivity().finish();
            } else {
                Log.e("Error:", "Request Error!");
            }
        }
    }

    public void openFileChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        initCameraPermission();
                        break;
                    case 1:
                        initGalleryIntent();
                        break;
                    default:
                }
            }
        });
        builder.show();
    }

    private void initGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initCameraPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(getContext(), "Permission to use Camera", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            initCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initCameraIntent();
            } else {
                Toast.makeText(getContext(), "Permission denied by user", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediafile(1);
        selectedImageUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File getOutputMediafile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name)
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyHHdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                realPath = selectedImageUri.getPath();
            } else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getContext(), data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(getContext(), data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(getContext(), data.getData());
                }
            }
//            Log.d("TAG", "real path: " + realPath);
            //Upload Image to Server
            new UploadTask().execute(realPath);
            mBitmap = ImageUtils.getScaledImage(selectedImageUri, getContext());
            setImageBitmap(mBitmap);
        }

    }

    private void setImageBitmap(Bitmap bm) {
        iv_image = (ImageView) view.findViewById(R.id.addImage);
        iv_image.setImageBitmap(bm);
    }

    private String uploadImage(String filePath) {
        OkHttpClient client = new OkHttpClient();
        try {
            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("images", filePath, RequestBody.create(MEDIA_TYPE_JPG, new File(filePath)))
                    .build();
            Request request = new Request.Builder()
                    .url("http://api.grosirbersama.co.id/upload")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (Exception e) {
            Log.e("TAG", "Error: "+e.getLocalizedMessage());
        }
        return null;
    }

    private class UploadTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            Toast.makeText(getContext(), "Please wait, uploading image to server...", Toast.LENGTH_LONG)
                    .show();
        }
        @Override
        protected String doInBackground(String... params) {
            String response = uploadImage(params[0]);
            if(response != null) {
                return response;
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            if(response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equals("success")) {
                        http_image = jsonObject.getString("path");
                    }
                } catch (JSONException je) {
                    Log.e("TAG", je.getLocalizedMessage());
                }
                Toast.makeText(getContext(), "Upload image sukses", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getContext(), "Upload image gagal!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

}