package com.shaladin.manager.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shaladin.manager.activity.categories.CategoriesActivity;
import com.shaladin.manager.MainActivity;
import com.shaladin.manager.activity.customers.CustomersActivity;
import com.shaladin.manager.activity.customers.ListCustomersActivity;
import com.shaladin.manager.activity.orders.OrdersActivity;
import com.shaladin.manager.activity.products.ProductsActivity;
import com.shaladin.manager.R;
import com.shaladin.manager.activity.settings.ChangePasswordActivity;
import com.shaladin.manager.activity.settings.PaymentsActivity;
import com.shaladin.manager.activity.settings.ProfileActivity;
import com.shaladin.manager.activity.settings.ReceiptActivity;
import com.shaladin.manager.activity.settings.SettingsActivity;
import com.shaladin.manager.activity.settings.StoreActivity;

/**
 * Created by EliteBook on 10/7/2016.
 */
public class CustomGridViewAdapter extends BaseAdapter {
    private Context context;
    private final String[] string;
    private final int[] imageId;
    private final String AuthPrefs = "AuthPrefs";
    private final String isAuth    = "isAuth";
    private final String UserId    = "UserId";
    private final String UserKey   = "UserKey";
    private final String Password  = "Password";

    public CustomGridViewAdapter(Context context, String[] string, int[] imageId) {
        this.context = context;
        this.string  = string;
        this.imageId = imageId;
    }

    @Override
    public int getCount() {
        return string.length;
    }

    @Override
    public Object getItem(int p) {
        return null;
    }

    @Override
    public long getItemId(int p) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View grid;
        LayoutInflater inflater     = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final SharedPreferences UserData  = context.getSharedPreferences("AuthPrefs", Context.MODE_PRIVATE);

        if (convertView == null) {
            grid = new View(context);
            grid = inflater.inflate(R.layout.gridview_custom_layout, null);
            TextView textView = (TextView) grid.findViewById(R.id.griview_text);
            ImageView imageView = (ImageView) grid.findViewById(R.id.gridview_image);
            textView.setText(string[position]);
            imageView.setImageResource(imageId[position]);
        } else {
            grid = (View) convertView;
        }

        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                switch (string[position]) {
                    case "Pesanan" :
                        Intent intent = new Intent(context, OrdersActivity.class);
                        context.startActivity(intent);
                        break;
                    case "Katalog" :
                        Intent katalog= new Intent(context, ProductsActivity.class);
                        context.startActivity(katalog);
                        break;
                    case "Kategori" :
                        Intent kategori = new Intent(context, CategoriesActivity.class);
                        context.startActivity(kategori);
                        break;
                    case "Pelanggan" :
                        Intent customer = new Intent(context, CustomersActivity.class);
                        context.startActivity(customer);
                        break;
                    case "Retail" :
                        Intent retail = new Intent(context, ListCustomersActivity.class);
                        retail.putExtra("Group", "Retail");
                        retail.putExtra("GroupId", 19);
                        retail.putExtra("CompanyId", UserData.getInt("CompanyId", 0));
                        context.startActivity(retail);
                        break;
                    case "Reseller" :
                        Intent reseller = new Intent(context, ListCustomersActivity.class);
                        reseller.putExtra("Group", "Reseller");
                        reseller.putExtra("GroupId", 18);
                        reseller.putExtra("CompanyId", UserData.getInt("CompanyId", 0));
                        context.startActivity(reseller);
                        break;
                    case "Distributor":
                        Intent distributor = new Intent(context, ListCustomersActivity.class);
                        distributor.putExtra("Group", "Distributor");
                        distributor.putExtra("GroupId", 17);
                        distributor.putExtra("CompanyId", UserData.getInt("CompanyId", 0));
                        context.startActivity(distributor);
                        break;
                    case "Pengaturan" :
                        Intent pengaturan = new Intent(context, SettingsActivity.class);
                        context.startActivity(pengaturan);
                        break;
                    case "Profile" :
                        Intent profile = new Intent(context, ProfileActivity.class);
                        context.startActivity(profile);
                        break;
                    case "Ganti Password" :
                        Intent changePassword = new Intent(context, ChangePasswordActivity.class);
                        context.startActivity(changePassword);
                        break;
                    case "Toko" :
                        Intent toko = new Intent(context, StoreActivity.class);
                        context.startActivity(toko);
                        break;
                    case "Nota" :
                        Intent nota = new Intent(context, ReceiptActivity.class);
                        context.startActivity(nota);
                        break;
                    case "Pembayaran" :
                        Intent pembayaran = new Intent(context, PaymentsActivity.class);
                        context.startActivity(pembayaran);
                        break;
                    case "Keluar" :
                        SharedPreferences AuthUser = context.getSharedPreferences(AuthPrefs, Context.MODE_PRIVATE);
                        SharedPreferences.Editor EditorUserAuth = AuthUser.edit();
                        EditorUserAuth.putBoolean(isAuth, false);
                        EditorUserAuth.putInt(UserId, 0);
                        EditorUserAuth.putString(UserKey, null);
                        EditorUserAuth.putString(Password, null);
                        EditorUserAuth.putInt("CompanyId", 0);
                        EditorUserAuth.putString("Company", "Store Manager");
                        EditorUserAuth.putString("Address", null);
                        EditorUserAuth.putBoolean("Token", false);
                        EditorUserAuth.commit();

                        Intent login = new Intent(context, MainActivity.class);
                        login.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        ((Activity) context).finish();
                        context.startActivity(login);
                        break;
                    default:
                        Toast.makeText(context, "Not available yet!", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

        return grid;
    }
}
