package com.shaladin.manager.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shaladin.manager.R;
import com.shaladin.manager.activity.products.ProductDetailActivity;
import com.shaladin.manager.holder.ProductsHolder;
import com.shaladin.manager.model.Product;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 9/2/2016.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsHolder> {
    private Context context;
    private List<Product> products;
    public final String EXTRA_PRODUCT_ID = "product_id";
    public final String EXTRA_PRODUCT = "product";
    public final String EXTRA_AMOUNT  = "amount";
    public final String EXTRA_IMAGE   = "image";
    public final String EXTRA_PRICE   = "price";
    public final String EXTRA_VIEWED  = "viewed";

    ArrayList<Product> productModel;

    public ProductsAdapter(Context context, List<Product> products) {
        this.context = context;
        this.products= products;
    }

    @Override
    public ProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products, parent, false);
        return new ProductsHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductsHolder holder, int position) {
        final Product product = products.get(position);
        final DecimalFormat formatter = new DecimalFormat("#,###");
        holder.productName.setText(product.getProductName());

        holder.productStock.setText("Stock: "+product.getStock());
        if(product.getStock() <= 10) {
            holder.productStock.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
        } else if (product.getStock() > 10 && product.getStock() <= 20) {
            holder.productStock.setTextColor(context.getResources().getColor(R.color.colorYellow));
        } else {
            holder.productStock.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }

        holder.productPrice.setText("Rp "+formatter.format(product.getPrice()));
        Picasso.with(context).load(product.getImage()).placeholder(R.drawable.placeholder)
                .fit()
                .into(holder.productImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra(EXTRA_PRODUCT_ID, product.getProductId());
                intent.putExtra(EXTRA_PRODUCT, product.getProductName());
                intent.putExtra(EXTRA_PRICE, "Rp "+formatter.format(product.getPrice()));
                intent.putExtra("base_price", product.getPrice());
                intent.putExtra(EXTRA_AMOUNT, product.getStock());
                intent.putExtra(EXTRA_IMAGE, product.getImage());
                intent.putExtra(EXTRA_VIEWED, product.getViewed());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != products ? products.size() : 0);
    }

    public void setFilter(List<Product> filterProducts) {
        productModel = new ArrayList<>();
        productModel.addAll(filterProducts);
        notifyDataSetChanged();
    }
}
