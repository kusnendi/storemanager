package com.shaladin.manager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shaladin.manager.R;
import com.shaladin.manager.model.Price;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by EliteBook on 3/21/2017.
 */
public class PricesAdapter extends RecyclerView.Adapter<PricesAdapter.PricesHolder> {

    private List<Price> priceList;
    private Context context;

    public PricesAdapter(Context context, List<Price> prices) {
        this.context  = context;
        this.priceList= prices;
    }

    @Override
    public PricesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prices, parent, false);
        return new PricesHolder(view);
    }

    @Override
    public void onBindViewHolder(PricesHolder holder, int position) {
        Price price = priceList.get(position);
        final DecimalFormat formatter = new DecimalFormat("#,###");

        holder.value.setText("Rp "+formatter.format(price.getPrice()));
        holder.type.setText("Fix Price");
        holder.quantity.setText(String.valueOf(price.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return (null != priceList ? priceList.size() : 0);
    }

    public static class PricesHolder extends RecyclerView.ViewHolder {
        protected TextView value;
        protected TextView type;
        protected TextView quantity;

        public PricesHolder(View view) {
            super(view);

            value       = (TextView) view.findViewById(R.id.value);
            type        = (TextView) view.findViewById(R.id.type);
            quantity    = (TextView) view.findViewById(R.id.quantity);
        }
    }
}
