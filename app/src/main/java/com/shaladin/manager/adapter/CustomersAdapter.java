package com.shaladin.manager.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.shaladin.manager.R;
import com.shaladin.manager.activity.customers.UserDetailActivity;
import com.shaladin.manager.config.ServiceUrl;
import com.shaladin.manager.holder.CustomerHolder;
import com.shaladin.manager.model.Customer;
import com.shaladin.manager.utils.HttpServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by EliteBook on 11/4/2016.
 */
public class CustomersAdapter extends RecyclerView.Adapter<CustomerHolder> {
    private Context context;
    private List<Customer> customers;
    private Customer _Customer;
    private int x;

    public CustomersAdapter(Context context, List<Customer> customers) {
        this.context    = context;
        this.customers  = customers;
    }

    @Override
    public CustomerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer, parent, false);
        return new CustomerHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomerHolder holder, final int position) {
        final Customer customer = customers.get(position);
        DecimalFormat formatter = new DecimalFormat("#,###");

        holder.realName.setText(customer.getFirstName() + " " + customer.getLastName());
        holder.description.setText(customer.getEmail());
        holder.balance.setText(formatter.format(customer.getBalance()));

//        holder.groupName.setText(customer.getGroupName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _Customer = customer; x = position;
                Intent detail = new Intent(context, UserDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("customer", _Customer);
                detail.putExtra("data", bundle);
                context.startActivity(detail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != customers ? customers.size() : 0);
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE :
                    try {
                        JSONObject data = new JSONObject();
                        data.put("user_id", _Customer.getUserId());

                        String activationUrl = ServiceUrl.USER_ACTIVATION;
                        new ActivationTask().execute(activationUrl, data.toString());
                    } catch (JSONException je) {
                        Log.i("TAG", je.getLocalizedMessage());
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE :
                    break;
            }
        }
    };

    private void showDialog(Customer customer) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(R.layout.dialog_customer_activation)
                .setNegativeButton("Batal", dialogClickListener)
                .setPositiveButton("Active", dialogClickListener)
                .create();
        dialog.show();

        EditText realName   = (EditText) dialog.findViewById(R.id.fullName);
        EditText email      = (EditText) dialog.findViewById(R.id.emailAddress);
        EditText group      = (EditText) dialog.findViewById(R.id.group);

        realName.setText(customer.getFirstName()+ " " +customer.getLastName());
        email.setText(customer.getEmail());
        group.setText(customer.getGroupName());
    }

    private class ActivationTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                String result = HttpServices.postData(params[0], params[1]);
                return result;
            } catch (Exception e) {
                Log.i(context.getString(R.string.app_name), e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(result != null) {
                customers.remove(_Customer);
                notifyItemRemoved(x);

                Toast.makeText(context, "Pelanggan berhasil di aktivasi!", Toast.LENGTH_LONG)
                        .show();
            }
            else {
                Toast.makeText(context, "Silahkan coba kembali !", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }
}
