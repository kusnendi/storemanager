package com.shaladin.manager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shaladin.manager.R;
import com.shaladin.manager.collection.PaymentCollection;
import com.shaladin.manager.model.Payment;

import java.util.List;

/**
 * Created by EliteBook on 10/28/2016.
 */
public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.PaymentHolder>{

    private Context context;
    private List<Payment> payments;

    public PaymentsAdapter(Context context, List<Payment> payments) {
        this.context = context;
        this.payments= payments;
    }

    @Override
    public PaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment, parent, false);
        return new PaymentHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentHolder vh, final int position) {
        final Payment payment = payments.get(position);
        vh.paymentName.setText(payment.getPaymentName());
        vh.description.setText(Html.fromHtml(payment.getInstructions()));

        vh.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payments.remove(payment);

                PaymentCollection paymentCollection = new PaymentCollection(context);
                paymentCollection.open();
                paymentCollection.delete(payment);

                notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != payments ? payments.size() : 0);
    }

    public class PaymentHolder extends RecyclerView.ViewHolder {
        public TextView paymentName;
        public TextView description;
        public ImageView edit;
        public ImageView delete;

        public PaymentHolder(View parent) {
            super(parent);

            paymentName = (TextView) parent.findViewById(R.id.paymentName);
            description = (TextView) parent.findViewById(R.id.description);
            delete = (ImageView) parent.findViewById(R.id.actionDelete);
            edit   = (ImageView) parent.findViewById(R.id.actionEdit);
        }
    }
}
