package com.shaladin.manager.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shaladin.manager.R;
import com.shaladin.manager.activity.orders.OrderDetailActivity;
import com.shaladin.manager.holder.OrdersHolder;
import com.shaladin.manager.model.Order;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by EliteBook on 9/1/2016.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersHolder> {
    private Context context;
    private List<Order> orderList;

    public OrdersAdapter(Context context, List<Order> orders) {
        this.context = context;
        this.orderList = orders;
    }
    @Override
    public OrdersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orders, parent, false);
        return new OrdersHolder(view);
    }
    @Override
    public void onBindViewHolder(OrdersHolder holder, int position) {
        final Order order = orderList.get(position);
        long timestamp = order.getTimestamp();
        Date date   = new Date(timestamp*1000L);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String _dateString = dateFormat.format(date);
        String orderHead   = "<small>#"+String.valueOf(order.getOrderId())+"</small>&nbsp;&nbsp;"
                +"<strong>"+_dateString+"</strong>";

        DecimalFormat numberFormat = new DecimalFormat("#,###");
        holder.orderId.setText(Html.fromHtml(orderHead));
        holder.status.setText(order.getStatus());
        holder.customerInfo.setText(order.getbFisrtname()+" - "+order.getbDistrict()+", "+order.getbCity());
        holder.subTotal.setText("Rp. "+numberFormat.format(order.getTotal()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("orderId", order.getOrderId());
                intent.putExtra("subtotal", order.getTotal());
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return (null != orderList ? orderList.size() : 0);
    }

}
