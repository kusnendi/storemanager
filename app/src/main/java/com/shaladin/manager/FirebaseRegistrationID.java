package com.shaladin.manager;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by EliteBook on 8/27/2016.
 */
public class FirebaseRegistrationID extends FirebaseInstanceIdService {
    private static final String TAG = "ServiceID";
    SharedPreferences AuthUser;

    @Override
    public void onTokenRefresh() {
        //Getting Registration Token
        String refreshToken = FirebaseInstanceId.getInstance().getToken();

//        AuthUser    = getSharedPreferences("UserPrefs", Context.MODE_PRIVATE);
//        int UserId  = AuthUser.getInt("user_id", 0);
        //Displaying Token in Logcat
        Log.e(TAG, "Token: "+refreshToken);
    }

    //Send token to server
    private void sendRegistrationToServer(String Token) {
        //
    }

    private class SendTokenTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            //
        }
    }
}
